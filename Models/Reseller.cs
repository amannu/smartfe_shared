﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using RestWS.Utils;

namespace RestWS.Models
{
    public class Reseller
{
    public int id { get; set; }
    public string RagioneSociale { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string PIva { get; set; }
    public string Note { get; set; }


        public Reseller() { }

        public Reseller(long id)
        {
            using (SqlConnection cn = ConfManager.GetDbConnection())
            {
                try
                {
                    cn.Open();
                    SqlCommand command = new SqlCommand("proc_sel_ResellerById", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@id", id));
                    using (SqlDataReader sdr = command.ExecuteReader())
                    {
                        initFromDataReader(sdr);
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.WriteException(ex);
                }
            }
        }


        private void initFromDataReader(SqlDataReader sdr)
        {
            if (sdr.Read())
            {
                if (sdr["id"] != DBNull.Value) this.id = (int)sdr["id"];
                if (sdr["RagioneSociale"] != DBNull.Value) this.RagioneSociale = (string)sdr["RagioneSociale"];
                if (sdr["Username"] != DBNull.Value) this.Username = (string)sdr["Username"];
                if (sdr["Password"] != DBNull.Value) this.Password = (string)sdr["Password"];
                if (sdr["PIva"] != DBNull.Value) this.PIva = (string)sdr["PIva"];
                if (sdr["Note"] != DBNull.Value) this.Note = (string)sdr["Note"];
            }
        }

    }
}