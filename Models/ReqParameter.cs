﻿using System;

namespace RestWS.Models
{
    public class ReqParameter
    {
        public string nome { get; set; }
        public Type tipo { get; set; }
    }

    public class CheckParametersResult
    {
        public bool success { get; set; }
        public string message { get; set; }
    }
}