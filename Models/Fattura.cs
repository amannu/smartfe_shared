﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestWS.Models
{
    public class FatturaModel
    {
        public long Id { get; set; }

        public long IdUser { get; set; }

        public string NomeFile { get; set; }

        public string ProgressivoInvio { get; set; }

        public string Denominazione { get; set; }

        public string IdFiscaleIva { get; set; }

        public string FormatoTrasmissione { get; set; }

        public string TipoDocumento { get; set; }

        public string NumeroFattura { get; set; }

        public DateTime Data { get; set; }

        public Decimal ImportoTotaleDocumento { get; set; }

        public string NomeFileMetadati { get; set; }

        public DateTime? DataPagamento { get; set; }

        public short IdStato { get; set; }

        public string StatoMittBreve { get; set; }

        public string StatoMittLungo { get; set; }

        public string StatoDestBreve { get; set; }

        public string StatoDestLungo { get; set; }

        public long? IdentificativoSDI { get; set; }

        public DateTime DataCreate { get; set; }

        public string IpCreate { get; set; }

        public string NoteInterne { get; set; }

        public long idLotto { get; set;  }
    }
}