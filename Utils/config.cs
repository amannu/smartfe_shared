﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using Newtonsoft.Json.Linq;
using RestWS.Models;

namespace RestWS.Utils
{
    public static class ConfManager
    {
        public static readonly bool DEBUG_MODE = true;

        //Configurazione locale Andrea
        //private static readonly string SQLSERVER_SERVER_NAME = @"UTENTE-PC\SQLEXPRESS";//@"IS-5B05226C\SQLEXPRESS";
        //private static readonly string SQLSERVER_DB_NAME = @"dbSmartFE";
        //private static readonly string SQLSERVER_USERNAME = @"ERepair_USR";//"sa";
        //private static readonly string SQLSERVER_PASSWORD = @"sgba137375";//"HfQd43:y0C";

        //Configurazione locale Giovanni
        //private static readonly string SQLSERVER_SERVER_NAME = @"GIOVANNI-ASUS\SQLEXPRESS";
        //private static readonly string SQLSERVER_DB_NAME = @"dbSmartFE";
        //private static readonly string SQLSERVER_USERNAME = @"ERepair_Commerciale";
        //private static readonly string SQLSERVER_PASSWORD = @"sgba137375";

        //Configurazione Thinkserver

        //private static readonly string SQLSERVER_SERVER_NAME = @"192.168.1.222";//@"IS-5B05226C\SQLEXPRESS";
        //private static readonly string SQLSERVER_DB_NAME = @"dbSmartFE";
        //private static readonly string SQLSERVER_USERNAME = @"ERepair_USR";//"sa";
        //private static readonly string SQLSERVER_PASSWORD = @"sgba137375";//"HfQd43:y0C";

        //Configurazione Coretech

        private static readonly string SQLSERVER_SERVER_NAME = @"IS-5B05226C\SQLEXPRESS";
        private static readonly string SQLSERVER_DB_NAME = @"dbSmartFE";
        private static readonly string SQLSERVER_USERNAME = "sa";
        private static readonly string SQLSERVER_PASSWORD = "HfQd43:y0C";

        private static volatile string cnStr;


        /// <summary>
        /// Restituisce la stringa di connessione
        /// </summary>
        /// <returns></returns>
        public static string getConnectionString()
        {
            if (String.IsNullOrEmpty(cnStr))
            {
                //Per accesso tramite IP

                //da me se metto Network Library=DBMSSOCN non si connette
                //cnStr = @"Server=%1;Network Library=DBMSSOCN;Initial Catalog=%2;User Id=%3;Password=%4;";
                cnStr = @"Server=%1;Initial Catalog=%2;User Id=%3;Password=%4;MultipleActiveResultSets=True";
                //cnStr = @"Server=%1;Initial Catalog=%2;User Id=%3;Password=%4;";
                //Per accesso tramite DOMAIN_NAME
                //string cnStr = @"Server=%1;Initial Catalog=%2;User Id=%3;Password=%4;";
                cnStr = cnStr.Replace("%1", SQLSERVER_SERVER_NAME).Replace("%2", SQLSERVER_DB_NAME)
                    .Replace("%3", SQLSERVER_USERNAME).Replace("%4", SQLSERVER_PASSWORD);

                //cnStr = "Server=GIOVANNI-ASUS\\SQLExpress;Initial Catalog=dbHorseMan;User Id=HorseMan_Usr;Password=sgba137375;";
                //cnStr = "Server=THINKSERVER;Initial Catalog=dbHorseMan;User Id=HorseMan_Usr;Password=sgba137375;";
                //cnStr = "Server=5SPACESV1\\SQLEXPRESS;Initial Catalog=dbBrandaniLeaflet;User Id=Brandani_USR;Password=sgb@137375;";
            }
            return cnStr;
        }

        /// <summary>
        /// Ritorna la variabile contenente le informazioni di configurazione
        /// Accesso concorrente permesso
        /// </summary>
        /// <returns></returns>
        public static SqlConnection GetDbConnection()
        {
            //if (cn == null)
            //    cn = newConnection();

            //if (cn.State == System.Data.ConnectionState.Closed)
            //{
            //    cn.ConnectionString = getConnectionString();
            //    return cn;
            //}
            //else
            return newConnection();
        }

        /// <summary>
        /// Crea e restituisce una nuova connessione con il db
        /// </summary>
        /// <returns></returns>
        private static SqlConnection newConnection()
        {
            string cs = getConnectionString();
            try
            {               
                SqlConnection cn = new SqlConnection(cs);
                cn.Open();
                //Ottimizza le prestazioni
                //Vedere http://www.sql-speed.com/2009/12/why-does-sql-server-management-studio_07.html
                //e http://stackoverflow.com/questions/2736638/sql-query-slow-in-net-application-but-instantaneous-in-sql-server-management-st
                using (SqlCommand comm = new SqlCommand("SET ARITHABORT ON", cn))
                {
                    comm.ExecuteNonQuery();
                }
                cn.Close();
                return cn;
            }
            catch (Exception)
            {
                LogWriter.WriteError("Connection string: " + cs);
                return null;
            }
        }


        
    }

    public partial class MyUtils
    {
        public static CheckParametersResult CheckParameters(List<ReqParameter> parameters, JObject data)
        {
            if (data == null) return new CheckParametersResult { success = false, message = "Oggetto dati null" };
            foreach (var parameter in parameters)
            {
                if (!((IDictionary<string, JToken>)data).ContainsKey(parameter.nome))
                {
                    return new CheckParametersResult { success = false, message = "Parametro mancante: " + parameter.nome };
                }
                //if (data[parameter.nome].GetType() != parameter.tipo)
                //{
                //    return new CheckParametersResult { success = false, message = "Parametro errato: " + parameter.nome };
                //}               
            }
            return new CheckParametersResult { success = true, message = "" };
        }

        //public static void WriteException(Exception ex, string additionalInfo = null)
        //{
        //    LogWriter.WriteException(ex, additionalInfo);
        //}

        public static string GetIPv4Address()
        {
            //StringBuilder sb = new StringBuilder();

            // Get a list of all network interfaces (usually one per network card, dialup, and VPN connection) 
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface network in networkInterfaces)
            {
                // Read the IP configuration for each network 
                IPInterfaceProperties properties = network.GetIPProperties();
                /*IPAddressCollection dnsServers = properties.DnsAddresses;
                if (dnsServers.Count > 0)
                {
                    foreach (IPAddress dns in dnsServers)
                    {
                        System.Diagnostics.Debug.Write("  DNS Servers ............................. : {0}",
                            dns.ToString());
                    }
                }*/
                // Each network interface may have multiple IP addresses 
                foreach (IPAddressInformation address in properties.UnicastAddresses)
                {
                    // We're only interested in IPv4 addresses for now 
                    if (address.Address.AddressFamily != AddressFamily.InterNetwork)
                        continue;

                    // Ignore loopback addresses (e.g., 127.0.0.1) 
                    if (IPAddress.IsLoopback(address.Address))
                        continue;

                    if (properties.GatewayAddresses.Count == 1)
                    {
                        return Dns.GetHostName() + " (" + address.Address + ")";
                        //sb.AppendLine(address.Address.ToString() + " (" + network.Name + ")");
                    }
                }
            }
            return Dns.GetHostName() + " (no IP)";
            //return sb.ToString();

            /*System.Net.Dns.GetHostName(); // The full TCP/IP based host name
            /* Will give you the NETBIOS name of the machine (restricted to 15 characters)
            System.Net.Dns.GetHostName();
            System.Environment.MachineName;*/
        }

        public static string convertNumberToReadableString(long num)
        {
            string result = "";
            long mod = 0;
            long i = 0;
            string[] unita = { "zero", "uno", "due", "tre", "quattro", "cinque", "sei", "sette", "otto", "nove", "dieci", "undici", "dodici", "tredici", "quattordici", "quindici", "sedici", "diciassette", "diciotto", "diciannove" };
            string[] decine = { "", "dieci", "venti", "trenta", "quaranta", "cinquanta", "sessanta", "settanta", "ottonta", "novanta" };
            if (num > 0 && num < 20)
            {

                result = unita[num];
            }
            else
            {
                if (num < 100)
                {
                    mod = num % 10;
                    i = num / 10;
                    switch (mod)
                    {
                        case 0:
                            result = decine[i];
                            break;
                        case 1:
                            result = decine[i].Substring(0, decine[i].Length - 1) + unita[mod];
                            break;
                        case 8:
                            result = decine[i].Substring(0, decine[i].Length - 1) + unita[mod];
                            break;
                        default:
                            result = decine[i] + unita[mod];
                            break;
                    }
                }
                else
                {
                    if (num < 1000)
                    {
                        mod = num % 100;
                        i = (num - mod) / 100;
                        switch (i)
                        {
                            case 1:
                                result = "cento";
                                break;
                            default:
                                result = unita[i] + "cento";
                                break;
                        }
                        result = result + convertNumberToReadableString(mod);
                    }
                    else
                    {
                        if (num < 10000)
                        {
                            mod = num % 1000;
                            i = (num - mod) / 1000;
                            switch (i)
                            {
                                case 1:
                                    result = "mille";
                                    break;
                                default:
                                    result = unita[i] + "mila";
                                    break;
                            }
                            result = result + convertNumberToReadableString(mod);
                        }
                        else
                        {
                            if (num < 1000000)
                            {
                                mod = num % 1000;
                                i = (num - mod) / 1000;
                                switch ((num - mod) / 1000)
                                {
                                    default:
                                        if (i < 20)
                                        {
                                            result = unita[i] + "mila";
                                        }
                                        else
                                        {
                                            result = convertNumberToReadableString(i) + "mila";
                                        }
                                        break;
                                }
                                result = result + convertNumberToReadableString(mod);
                            }
                            else
                            {
                                if (num < 1000000000)
                                {
                                    mod = num % 1000000;
                                    i = (num - mod) / 1000000;
                                    switch (i)
                                    {
                                        case 1:
                                            result = "unmilione";
                                            break;

                                        default:
                                            result = convertNumberToReadableString(i) + "milioni";

                                            break;
                                    }
                                    result = result + convertNumberToReadableString(mod);
                                }
                                else
                                {
                                    if (num < 1000000000000)
                                    {
                                        mod = num % 1000000000;
                                        i = (num - mod) / 1000000000;
                                        switch (i)
                                        {
                                            case 1:
                                                result = "unmiliardo";
                                                break;

                                            default:
                                                result = convertNumberToReadableString(i) + "miliardi";

                                                break;
                                        }
                                        result = result + convertNumberToReadableString(mod);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return result;

        }

        //public static Size GetThumbnailSize(Image original, double? width, double? height)
        //{

        //    if (width == null) width = 0;
        //    if (height == null) height = 0;

        //    if (width == 0 && height == 0) return new Size(original.Width, original.Height);

        //    if (width != 0 && height == 0)
        //    {
        //        double newW = original.Width * ((double)width / original.Width);
        //        double newH = original.Height * ((double)width / original.Width);
        //        return new Size((int)newW, (int)newH);
        //    }

        //    if (width == 0 & height != 0)
        //    {
        //        double newW = original.Width * ((double)height / original.Height);
        //        double newH = original.Height * ((double)height / original.Height);
        //        return new Size((int)newW, (int)newH);
        //    }

        //    return new Size((int)width, (int)height);
        //}

        //public static byte[] imageToByteArray(Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
        //    return ms.ToArray();
        //}

        //public static Bitmap ResizeImage(Image image, int width, int height)
        //{
        //    var destRect = new Rectangle(0, 0, width, height);
        //    var destImage = new Bitmap(width, height);

        //    destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        //    using (var graphics = Graphics.FromImage(destImage))
        //    {
        //        graphics.CompositingMode = CompositingMode.SourceCopy;
        //        graphics.CompositingQuality = CompositingQuality.HighQuality;
        //        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
        //        graphics.SmoothingMode = SmoothingMode.HighQuality;
        //        graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

        //        using (var wrapMode = new ImageAttributes())
        //        {
        //            wrapMode.SetWrapMode(WrapMode.TileFlipXY);
        //            graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
        //        }
        //    }

        //    return destImage;
        //}



    }
}