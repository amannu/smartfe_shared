﻿using System;
using System.Collections.Generic;

namespace RestWS.Models
{
	
	    public class MessaggioSDI
    {       
        public long id { get; set; }
        public long IdentificativoSDI { get; set; }
        public short TipoFatt { get; set; }
        public string NomeFileNotifica { get; set; }
        public string NomeFileFattura { get; set; }
        public short idTipoMessaggio { get; set; }
        public DateTime DataRicezione { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public string Note { get; set; }
        public bool Letto { get; set; }
        public List<ErroriSDI> Errori { get; set; }
	    public short tipoFatt { get; set; }

    }

    public class ErroriSDI
    {
        public string Codice { get; set; }
        public string Descrizione { get; set; }
        public string Suggerimento { get; set; }
    }

    public class TipoMessaggioSDI
    {
        public short id { get; set; }
        public string Descrizione { get; set; }
        public string Titolo { get; set; }
    }
}