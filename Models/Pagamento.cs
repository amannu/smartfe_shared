﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestWS.Models
{
    public class Pagamento
    {
        public long id { get; set; }
        public long idUser { get; set; }
        public int idPianoCommerciale { get; set; }
        public string Descrizione { get; set; }
        public decimal Imponibile { get; set; }
        public decimal Importo { get; set; }
        public DateTime Scadenza { get; set; }
        public DateTime? DataPagamento { get; set; }
        public string TipoPagamento { get; set; }

        public string CausaleBonifico
        {
            get { return "Pagamento SmartFE " + id; }
        }

        public string NumeroFattura { get; set; }
        public DateTime? DataFattura { get; set; }
        public bool Alert { get; set; }
        public string Note { get; set; }

        public short Stato
        {
            get
            {
                if (DataPagamento != null) return 3;
                if ((Scadenza < DateTime.Now)) return 1;
                if ((Scadenza - DateTime.Now).TotalDays < 30) return 2;                
                return 0;
            }
        }
    }

}