﻿using System;

namespace RestWS.RequestModels
{

    public class RequestLogin
    {
        /// <summary>Username, richiesto</summary>
        public string Username { get; set; }
        /// <summary>Password, richiesto</summary>
        public string Password { get; set; }
    }
    
    /// <summary>Per registrare un utente è necessario che abbia inserito nel proprio cassetto fiscale il CodiceDestinatario (disponibile a breve) </summary>
    public class RequestRegister : RequestLogin
    {
        /// <summary>Nome del referente dell'utente, opzionale</summary>
        public string NomeReferente { get; set; }
        /// <summary>Cognome del referente dell'utente, opzionale</summary>
        public string CognomeReferente { get; set; }
        /// <summary>Ragione Sociale dell'utente, opzionale</summary>
        public string RagioneSociale { get; set; }
        /// <summary>Partita iva dell'utente, opzionale</summary>
        public string PartitaIva { get; set; }
        /// <summary>Indirizzo dell'utente, opzionale</summary>
        public string Indirizzo { get; set; }
        /// <summary>Cap dell'utente, opzionale</summary>
        public string Cap { get; set; }
        /// <summary>Citta dell'utente, opzionale</summary>
        public string Citta { get; set; }
        /// <summary>Telefono dell'utente, opzionale</summary>
        public string Telefono { get; set; }
        /// <summary>Email dell'utente, opzionale</summary>
        public string Email { get; set; }
        /// <summary>Pec dell'utente, opzionale</summary>
        public string Pec { get; set; }
        /// <summary>Note generiche, opzionale</summary>
        public string Note { get; set; }
    }

    public class WebRequestRegister : RequestRegister
    {
        /// <summary>Id del piano commerciale, richiesto</summary>
        public int IdPianoCommerciale { get; set; }
        /// <summary>Id del reseller inserire 0 per 5Space, richiesto</summary>
        public int idReseller { get; set; }   
    }

    public class PartnerRequestRegister : RequestRegister
    {
        /// <summary>Username del partner commerciale, richiesto</summary>
        public string PartnerName { get; set; }
        /// <summary>Password del partner commerciale, richiesto</summary>
        public string PartnerPassword { get; set; }
    }

    public class RequestInserisciPinUtente : RequestLogin
    {
        /// <summary>Pin, richiesto</summary>
        public string Pin { get; set; }
    }

    public class RequestMarcaMessaggioLetto : RequestLogin
    {
        /// <summary>Id del messaggio, richiesto</summary>
        public long MessageId { get; set; }
    }

    public class RequestInviaNotificaEsito : RequestLogin
    {
        /// <summary>Nome del file, richiesto</summary>
        public string FileName { get; set; }
        /// <summary>File xml della risposta in base64, richiesto</summary>
        public string File { get; set; }
        /// <summary>IdentificativoSdi della fattura, richiesto</summary>
        public string identificativoSdi { get; set; }
    }

    public class RequestCaricaInviaFattura : RequestLogin
    {
        /// <summary>Nome del file, richiesto</summary>
        public string FileName { get; set; }
        /// <summary>File xml della fattura in base46, richiesto</summary>
        public string File { get; set; }
        /// <summary>Inserire FPR12 per fatture b2b (fatture PA non gestite al momento), richiesto</summary>
        public string FormatoTrasmissione { get; set; }
    }

    public class RequestFattura : RequestLogin
    {
        /// <summary>Id della fattura, richiesto</summary>
        public long IdFattura { get; set; }
    }

    public class RequestInviaLottoFatture : RequestLogin
    {
        /// <summary>Id del lotto, richiesto</summary>
        public long IdLotto { get; set; }
    }

    public class RequestDataInterval : RequestLogin
    {
        /// <summary>Data inizio, opzionale</summary>
        public DateTime? StartDate { get; set; }
        /// <summary>Data fine, opzionale</summary>
        public DateTime? EndDate { get; set; }
    }

    public class RequestDocumentiAttivi : RequestDataInterval
    {
        public short idStato { get; set; }
    }
}