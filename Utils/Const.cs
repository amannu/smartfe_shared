﻿using System;

namespace RestWS.Utils
{
    public static class Const
    {
        public static short DA_INVIARE_STATE = 1; //per le fatture b2b e PA
        public static short IN_ELABORAZIONE_SDI_STATE = 2; //per le fatture b2b e PA
        public static short SCARTATA_STATE = 3; //per le fatture b2b e PA
        public static short NON_CONSEGNATA_STATE = 4; //per le fatture b2b e PA
        public static short CONSEGNATA_STATE = 5; //per le fatture b2b       
        public static short ATTESA_ACCETTAZIONE_STATE = 6; //per le fatture PA
        public static short ACCETTATA_STATE = 7; //per le fatture PA
        public static short RIFIUTATA_STATE = 8; //per le fatture PA
        public static short DECORRENZA_TERMINI_STATE = 9; //per le fatture PA
        public static short IMPOSSIBILITA_RECAPITO_STATE = 10; //per le fatture PA
        
        public static short NOTIFICA_RICEVUTA_CONSEGNA_TYPE = 1;
        public static short NOTIFICA_SCARTO_TYPE = 2;
        public static short NOTIFICA_MANCATA_CONSEGNA_TYPE = 3;
        public static short NOTIFICA_ESITO_TYPE = 4;
        public static short NOTIFICA_DECORRENZA_TERMINI_TYPE = 5;
        public static short NOTIFICA_ATTESTAZIONE_TRASMISSIONE_FATTURA_TYPE = 6;
        

        public static string RICEVUTA_CONSEGNA_TITLE = "Ricevuta di consegna";
        public static string RICEVUTA_CONSEGNA_DESCRIPTION = "Descrizione ricevuta di consegna";

        
        

        
        

        
        
        

        
        

        
        



        public const string KEY_FILENAME = "app_key.key";
        /// <summary>
        /// Password per la criptazione del file di configurazione e quello del PRODUCT_KEY
        /// </summary>
        public const string KEY_PASSWORD = "$£&%(//=uythnùè+ù";
        /// <summary>
        /// Nome del programma corrente
        /// </summary>
        public static readonly string ASSEMBLY_NAME = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        /// <summary>
        /// Versione dell'applicazione eseguita la volta precedente a quella corrente.
        /// Usata per visualizzare il changelog quando cambia la versione
        /// </summary>
        public static string LAST_APP_VERSION = "";
        /// <summary>
        /// Path corrente della cartella dei documenti
        /// </summary>
        public static string DOCUMENT_PATH = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"5Space\" + ASSEMBLY_NAME + @"\");
        public const string DB_FILES_PATH_FIELD = "FILES_PATH";
        public const string FATTURE_ATTIVE_FOLDER = "FattureAttive";
        public const string FATTURE_PASSIVE_FOLDER = "FatturePassive";

    }
}