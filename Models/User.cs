﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using RestWS.Utils;

namespace RestWS.Models
{
    public class User
    {
        public long id { get; set; }
        public int stato { get; set; }
        public int idReseller { get; set; }
        public int idPartner { get; set; }
        public DateTime CreateDate { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Pin { get; set; }
        public string Logo { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Denominazione { get; set; }
        public int idPianoCommerciale { get; set; }
        public string IdFiscaleIVA { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Pec { get; set; }
        public string Note { get; set; }
        public DateTime? LastAccessDate { get; set; }
        public bool Active { get; set; }
        public int QuotaFatture { get; set; }
        public int TotFatture { get; set; }
        public bool EasyReader { get; set; }
        public DateTime? DataInizioSottoscrizione { get; set; }
        public DateTime? DataFineSottoscrizione { get; set; }
		public DateTime? DataFineQuota { get; set; }
        public string NoteRivenditore { get; set; }
        public string PivaRivenditore { get; set; }
        public string RagioneSocialeRivenditore { get; set; }
        public string DescrizionePianoCommerciale { get; set; }
        public string NomePianoCommerciale { get; set; }
        public int NumeroDocumentiPianoCommerciale { get; set; }
        public decimal PrezzoPianoCommerciale { get; set; }
        public decimal PrezzoEasyReader { get; set; }
        public string RinnovoPianoCommerciale { get; set; }
        public string StatoUtente { get; set; }
        public List<Pagamento> Scadenze { get; set; }
        public List<Pagamento> Pagamenti { get; set; }

        public User() { }

        public User(long id)
        {
            using (SqlConnection cn = ConfManager.GetDbConnection())
            {
                try
                {
                    cn.Open();
                    SqlCommand command = new SqlCommand("proc_sel_UserById", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@idUser", id));
                    using (SqlDataReader sdr = command.ExecuteReader())
                    {
                        initFromDataReader(sdr);
                        getScadenze();
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.WriteException(ex);
                }
            }
        }

        public User(string idFiscale)
        {
            using (SqlConnection cn = ConfManager.GetDbConnection())
            {
                try
                {
                    cn.Open();
                    SqlCommand command = new SqlCommand("proc_sel_UserByIdFiscale", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@idFiscaleIva", idFiscale));
                    using (SqlDataReader sdr = command.ExecuteReader())
                    {
                        initFromDataReader(sdr);
                        getScadenze();
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.WriteException(ex);
                }
            }
        }

        public User(string Username, string Password)
        {
            using (SqlConnection cn = ConfManager.GetDbConnection())
            {
                try
                {
                    cn.Open();
                    SqlCommand command = new SqlCommand("proc_sel_UserByUsernameAndPassword", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Username", Username));
                    command.Parameters.Add(new SqlParameter("@Password", Password));
                    using (SqlDataReader sdr = command.ExecuteReader())
                    {
                        initFromDataReader(sdr);
                        getScadenze();
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.WriteException(ex);
                }
            }
        }

        private void initFromDataReader(SqlDataReader sdr)
        {
            if (sdr.Read())
            {
                if (sdr["id"] != DBNull.Value) this.id = (long)sdr["id"];
                if (sdr["stato"] != DBNull.Value) this.stato = (int)sdr["stato"];
                if (sdr["idReseller"] != DBNull.Value) this.idReseller = (int)sdr["idReseller"];
                if (sdr["idPartner"] != DBNull.Value) this.idPartner = (int)sdr["idPartner"];
                if (sdr["CreateDate"] != DBNull.Value) this.CreateDate = (DateTime)sdr["CreateDate"];
                if (sdr["Username"] != DBNull.Value) this.Username = (string)sdr["Username"];
                if (sdr["Password"] != DBNull.Value) this.Password = (string)sdr["Password"];
                if (sdr["Pin"] != DBNull.Value) this.Pin = (string)sdr["Pin"];
                if (sdr["Logo"] != DBNull.Value) this.Logo = (string)sdr["Logo"];
                if (sdr["Name"] != DBNull.Value) this.Name = (string)sdr["Name"];
                if (sdr["Surname"] != DBNull.Value) this.Surname = (string)sdr["Surname"];
                if (sdr["Denominazione"] != DBNull.Value) this.Denominazione = (string)sdr["Denominazione"];
                if (sdr["idPianoCommerciale"] != DBNull.Value) this.idPianoCommerciale = (int)sdr["idPianoCommerciale"];
                if (sdr["IdFiscaleIVA"] != DBNull.Value) this.IdFiscaleIVA = (string)sdr["IdFiscaleIVA"];
                if (sdr["Address"] != DBNull.Value) this.Address = (string)sdr["Address"];
                if (sdr["PostCode"] != DBNull.Value) this.PostCode = (string)sdr["PostCode"];
                if (sdr["City"] != DBNull.Value) this.City = (string)sdr["City"];
                if (sdr["Country"] != DBNull.Value) this.Country = (string)sdr["Country"];
                if (sdr["Telefono"] != DBNull.Value) this.Telefono = (string)sdr["Telefono"];
                if (sdr["Email"] != DBNull.Value) this.Email = (string)sdr["Email"];
                if (sdr["Pec"] != DBNull.Value) this.Pec = (string)sdr["Pec"];
                if (sdr["Note"] != DBNull.Value) this.Note = (string)sdr["Note"];
                if (sdr["LastAccessDate"] != DBNull.Value) this.LastAccessDate = (DateTime)sdr["LastAccessDate"];
                if (sdr["Active"] != DBNull.Value) this.Active = (bool)sdr["Active"];
                if (sdr["QuotaFatture"] != DBNull.Value) this.QuotaFatture = (int)sdr["QuotaFatture"];
                if (sdr["TotFatture"] != DBNull.Value) this.TotFatture = (int)sdr["TotFatture"];
                if (sdr["EasyReader"] != DBNull.Value) this.EasyReader = (bool)sdr["EasyReader"];
                if (sdr["DataInizioSottoscrizione"] != DBNull.Value) this.DataInizioSottoscrizione = (DateTime)sdr["DataInizioSottoscrizione"];
                if (sdr["DataFineSottoscrizione"] != DBNull.Value) this.DataFineSottoscrizione = (DateTime)sdr["DataFineSottoscrizione"];
				if (sdr["DataFineQuota"] != DBNull.Value) this.DataFineQuota = (DateTime)sdr["DataFineQuota"];
                //if (sdr["NoteRivenditore"] != DBNull.Value) this.NoteRivenditore = (string)sdr["NoteRivenditore"];
                //if (sdr["PivaRivenditore"] != DBNull.Value) this.PivaRivenditore = (string)sdr["PivaRivenditore"];
                //if (sdr["RagioneSocialeRivenditore"] != DBNull.Value) this.RagioneSocialeRivenditore = (string)sdr["RagioneSocialeRivenditore"];
                if (sdr["DescrizionePianoCommerciale"] != DBNull.Value) this.DescrizionePianoCommerciale = (string)sdr["DescrizionePianoCommerciale"];
                if (sdr["NomePianoCommerciale"] != DBNull.Value) this.NomePianoCommerciale = (string)sdr["NomePianoCommerciale"];
                if (sdr["NumeroDocumentiPianoCommerciale"] != DBNull.Value) this.NumeroDocumentiPianoCommerciale = (int)sdr["NumeroDocumentiPianoCommerciale"];
                if (sdr["PrezzoPianoCommerciale"] != DBNull.Value) this.PrezzoPianoCommerciale = (decimal)sdr["PrezzoPianoCommerciale"];
                if (sdr["PrezzoEasyReader"] != DBNull.Value) this.PrezzoEasyReader = (decimal)sdr["PrezzoEasyReader"];
                if (sdr["RinnovoPianoCommerciale"] != DBNull.Value) this.RinnovoPianoCommerciale = (string)sdr["RinnovoPianoCommerciale"];
                if (sdr["StatoUtente"] != DBNull.Value) this.StatoUtente = (string)sdr["StatoUtente"];
            }
        }

        private void getScadenze()
        {
            using (SqlConnection cn = ConfManager.GetDbConnection())
            {
                try
                {
                    Scadenze = new List<Pagamento>();
                    Pagamenti = new List<Pagamento>();
                    cn.Open();
                    SqlCommand command = new SqlCommand("proc_sel_ScadenzeUtente", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@idUser", id));
                    command.Parameters.Add(new SqlParameter("@mostraPagati", 1));
                    using (SqlDataReader sdr = command.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Pagamento pagamento = new Pagamento();
                            if (sdr["id"] != DBNull.Value) pagamento.id = (long)sdr["id"];
                            if (sdr["idUser"] != DBNull.Value) pagamento.idUser = (long)sdr["idUser"];
                            if (sdr["idPianoCommerciale"] != DBNull.Value) pagamento.idPianoCommerciale = (int)sdr["idPianoCommerciale"];
                            if (sdr["Descrizione"] != DBNull.Value) pagamento.Descrizione = (string)sdr["Descrizione"];
                            if (sdr["Imponibile"] != DBNull.Value) pagamento.Imponibile = (decimal)sdr["Imponibile"];
                            if (sdr["Importo"] != DBNull.Value) pagamento.Importo = (decimal)sdr["Importo"];
                            if (sdr["Scadenza"] != DBNull.Value) pagamento.Scadenza = (DateTime)sdr["Scadenza"];
                            if (sdr["DataPagamento"] != DBNull.Value) pagamento.DataPagamento = (DateTime)sdr["DataPagamento"];
                            if (sdr["TipoPagamento"] != DBNull.Value) pagamento.TipoPagamento = (string)sdr["TipoPagamento"];
                            if (sdr["NumeroFattura"] != DBNull.Value) pagamento.NumeroFattura = (string)sdr["NumeroFattura"];
                            if (sdr["DataFattura"] != DBNull.Value) pagamento.DataFattura = (DateTime)sdr["DataFattura"];
                            if (sdr["Alert"] != DBNull.Value) pagamento.Alert = (bool)sdr["Alert"];
                            if (sdr["Note"] != DBNull.Value) pagamento.Note = (string)sdr["Note"];

                            if (pagamento.DataPagamento == null)
                                Scadenze.Add(pagamento);
                            else Pagamenti.Add(pagamento);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.WriteException(ex);
                }
            }
        }
    }
}