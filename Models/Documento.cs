﻿using RestWS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestWS.Models
{
    public class Documento
    {
        public bool IsLotto { get; set; }

        public Lotto Lotto { get; set; }

        public FatturaModel Fattura { get; set; }
    }
}