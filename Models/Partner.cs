﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using RestWS.Utils;

namespace RestWS.Models
{
    public class Partner
{
    public int id { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string Nome { get; set; }
    public string PIva { get; set; }
    public string Note { get; set; }
    public int? QuotaFatture { get; set; }
    public int? TotFatture { get; set; }
    public DateTime? DataInizioSottoscrizione { get; set; }
    public DateTime? DataFineSottoscrizione { get; set; }
    public DateTime? DataFineQuota { get; set; }
    public int QuotaUtenti { get; set; }
    public int TotUtenti { get; set; }


        public Partner() { }

        public Partner(long id)
        {
            using (SqlConnection cn = ConfManager.GetDbConnection())
            {
                try
                {
                    cn.Open();
                    SqlCommand command = new SqlCommand("proc_sel_PartnerById", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@id", id));
                    using (SqlDataReader sdr = command.ExecuteReader())
                    {
                        initFromDataReader(sdr);
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.WriteException(ex);
                }
            }
        }

        public Partner(string Username, string Password)
        {
            using (SqlConnection cn = ConfManager.GetDbConnection())
            {
                try
                {
                    cn.Open();
                    SqlCommand command = new SqlCommand("proc_sel_PartnerByUsernameAndPassword", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Username", Username));
                    command.Parameters.Add(new SqlParameter("@Password", Password));
                    using (SqlDataReader sdr = command.ExecuteReader())
                    {
                        initFromDataReader(sdr);
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.WriteException(ex);
                }
            }
        }



        private void initFromDataReader(SqlDataReader sdr)
        {
            if (sdr.Read())
            {
                if (sdr["id"] != DBNull.Value) this.id = (int)sdr["id"];
                if (sdr["Username"] != DBNull.Value) this.Username = (string)sdr["Username"];
                if (sdr["Password"] != DBNull.Value) this.Password = (string)sdr["Password"];
                if (sdr["Nome"] != DBNull.Value) this.Nome = (string)sdr["Nome"];
                if (sdr["PIva"] != DBNull.Value) this.PIva = (string)sdr["PIva"];
                if (sdr["Note"] != DBNull.Value) this.Note = (string)sdr["Note"];
                if (sdr["QuotaFatture"] != DBNull.Value) this.QuotaFatture = (int)sdr["QuotaFatture"];
                if (sdr["TotFatture"] != DBNull.Value) this.TotFatture = (int)sdr["TotFatture"];
                if (sdr["DataInizioSottoscrizione"] != DBNull.Value) this.DataInizioSottoscrizione = (DateTime)sdr["DataInizioSottoscrizione"];
                if (sdr["DataFineSottoscrizione"] != DBNull.Value) this.DataFineSottoscrizione = (DateTime)sdr["DataFineSottoscrizione"];
                if (sdr["DataFineQuota"] != DBNull.Value) this.DataFineQuota = (DateTime)sdr["DataFineQuota"];
                if (sdr["QuotaUtenti"] != DBNull.Value) this.QuotaUtenti = (int)sdr["QuotaUtenti"];
                if (sdr["TotUtenti"] != DBNull.Value) this.TotUtenti = (int)sdr["TotUtenti"];
            }
        }

    }
}