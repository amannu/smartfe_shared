﻿using System;
using System.Data;
using System.Data.SqlClient;
using RestWS.Utils;

namespace RestWS.Models
{
    public class Codifica
    {
        public string Cod { get; set; }
        public string Descrizione { get; set; }
        public string Tipo { get; set; }


        public Codifica(string cod)
        {
            using (SqlConnection cn = ConfManager.GetDbConnection())
            {
                try
                {
                    cn.Open();
                    SqlCommand command = new SqlCommand("proc_sel_Codifica", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Cod", cod));
                    using (SqlDataReader sdr = command.ExecuteReader())
                    {
                        if (sdr.Read())
                        {
                            if (sdr["Cod"] != DBNull.Value) this.Cod = (string)sdr["Cod"];
                            if (sdr["Descrizione"] != DBNull.Value) this.Descrizione = (string)sdr["Descrizione"];
                            if (sdr["Tipo"] != DBNull.Value) this.Tipo = (string)sdr["Tipo"];
                        }
                    }
                }
                catch (Exception ex)
                {
                    //GlobalUtils.WriteException(ex);
                    //Mouse.OverrideCursor = null;
                    //MessageBox.Show(ex.Message, "Errore", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
                }
            }
        }
    }
}