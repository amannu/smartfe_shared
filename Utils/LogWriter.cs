﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace RestWS.Utils
{
    public class LogWriter
    {
        private static void LogWrite(string logMessage, string type)
        {
            try
            {
                string Filename = getFileName(type);
                if (!Directory.Exists(Path.GetDirectoryName(Filename)))
                {
                    DirectoryInfo di = Directory.CreateDirectory(Path.GetDirectoryName(Filename));
                }

                using (StreamWriter w = File.AppendText(Filename))
                {
                    w.Write("\r\n[");
                    w.Write("{0}", DateTime.Now.ToLongTimeString());
                    w.Write("]: ");
                    w.WriteLine(logMessage);
                    w.Close();                    
                    FtpSend(Filename);
                }
            }
            catch (Exception)
            {
            }
        }

        private static void FtpSend(string Filename)
        {
            //TODO
        }


        private static string getFileName(string type)
        {
            return "C:\\log\\" + DateTime.Now.ToString("yyyyMMdd") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "_" + type + ".log";
        }

        public static void WriteException(Exception ex, string additionalInfo = null)
        {
            ex = ex.GetBaseException();
            string message = "";
            //message += ("\r\n\r\n\r\n");
            message += ("********************** ECCEZIONE " + ex.GetType() + "**********************\r\n");
            if (!String.IsNullOrWhiteSpace(additionalInfo))
                message += ("Id Operazione: " + additionalInfo + "\r\n");
            message += ("Message: " + ex.Message);
            StackTrace trace = new StackTrace(ex, true);
            if (trace != null && trace.GetFrame(0) != null)
            {
                message += ("File: " + trace.GetFrame(0).GetFileName());
                message += ("Codice: " + trace.GetFrame(0).GetMethod().Name);
                message += ("Line number: " + trace.GetFrame(0).GetFileLineNumber());
                message += ("Colonna: " + trace.GetFrame(0).GetFileColumnNumber());
                message += ("StackTrace: " + ex.StackTrace);
            }
            message += ("\r\n");
            message += ("**********************FINE ECCEZIONE " + ex.GetType() + "**********************\r\n\r\n\r\n\r\n");
            LogWrite(message, "errors");
        }

        public static void WriteError(string errore, string additionalInfo = null)
        {
            string message = "";
            //message += ("\r\n\r\n\r\n");
            message += ("********************** ERRORE **********************\r\n");
            if (!String.IsNullOrWhiteSpace(additionalInfo))
                message += ("Id Operazione: " + additionalInfo + "\r\n");
            message += ("Message: \r\n" + errore);
            message += ("\r\n");
            message += ("**********************FINE ERRORE **********************\r\n\r\n\r\n\r\n");
            LogWrite(message, "errors");
        }


        public static void WriteLog(string header, string type, object[] obj)
        {
            try
            {
                string message = "** " + header + " **";
                message += ("\r\n");
                message += JsonConvert.SerializeObject(obj);
                //message += ("\r\n");

                LogWrite(message, type);
            }
            catch (Exception)
            {

            }
        }

        public static void WriteLog(string header, string type, string log)
        {
            try
            {
                string message = "** " + header + " **";
                message += ("\r\n");
                message += log;
                //message += ("\r\n");

                LogWrite(message, type);
            }
            catch (Exception)
            {

            }
        }
    }
}