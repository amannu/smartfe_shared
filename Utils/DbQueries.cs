﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using FatturaElettronica;
using FatturaElettronica.FatturaElettronicaHeader.CessionarioCommittente;
using RestWS.Models;
using RestWS.RequestModels;


namespace RestWS.Utils
{
    public class DbQueries
    {
        public static string GetConfigField(string field)
        {
            SqlDataReader rdr = null;
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    const string cmdString =
                    "SELECT value FROM tbConfig WHERE id = @field";
                    var cmd = new SqlCommand(cmdString, con);
                    cmd.Parameters.Add(new SqlParameter("@field", field));
                    rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {
                        return Convert.ToString(rdr[0]);
                    }

                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
            }
            return null;
        }

        public static List<Documento> getDocumentiAttivi(long userId, RequestDocumentiAttivi request)
        {
            SqlTransaction myTran = null;

            List<Documento> documenti = new List<Documento>();
            List<Lotto> lotti = new List<Lotto>();
            List<FatturaModel> fatture = new List<FatturaModel>();
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    myTran = con.BeginTransaction();
                    var cmd = new SqlCommand("proc_sel_FattureAttive", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = myTran;
                    cmd.Parameters.Add(new SqlParameter("@idUser", userId));
                    if (request.StartDate != null) cmd.Parameters.Add(new SqlParameter("@startDate", request.StartDate));
                    if (request.EndDate != null) cmd.Parameters.Add(new SqlParameter("@endDate", request.EndDate));
                    cmd.Parameters.Add(new SqlParameter("@idStato", request.idStato));


                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            FatturaModel fattura = new FatturaModel();
                            if (sdr["id"] != DBNull.Value) fattura.Id = (long)sdr["id"];
                            if (sdr["idUser"] != DBNull.Value) fattura.IdUser = (long)sdr["idUser"];
                            if (sdr["NomeFile"] != DBNull.Value) fattura.NomeFile = (string)sdr["NomeFile"];
                            if (sdr["ProgressivoInvio"] != DBNull.Value) fattura.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
                            if (sdr["Denominazione"] != DBNull.Value) fattura.Denominazione = (string)sdr["Denominazione"];
                            if (sdr["IdFiscaleIVA"] != DBNull.Value) fattura.IdFiscaleIva = (string)sdr["IdFiscaleIVA"];
                            if (sdr["FormatoTrasmissione"] != DBNull.Value) fattura.FormatoTrasmissione = (string)sdr["FormatoTrasmissione"];
                            if (sdr["TipoDocumento"] != DBNull.Value) fattura.TipoDocumento = (string)sdr["TipoDocumento"];
                            if (sdr["NumeroFattura"] != DBNull.Value) fattura.NumeroFattura = (string)sdr["NumeroFattura"];
                            if (sdr["Data"] != DBNull.Value) fattura.Data = (DateTime)sdr["Data"];
                            if (sdr["ImportoTotaleDocumento"] != DBNull.Value) fattura.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
                            if (sdr["NomeFileMetadati"] != DBNull.Value) fattura.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
                            if (sdr["DataPagamento"] != DBNull.Value) fattura.DataPagamento = (DateTime)sdr["DataPagamento"];
                            if (sdr["idStato"] != DBNull.Value) fattura.IdStato = (short)sdr["idStato"];
                            if (sdr["IdentificativoSdI"] != DBNull.Value) fattura.IdentificativoSDI = (long)sdr["IdentificativoSdI"];
                            if (sdr["DataCreate"] != DBNull.Value) fattura.DataCreate = (DateTime)sdr["DataCreate"];
                            if (sdr["IpCreate"] != DBNull.Value) fattura.IpCreate = (string)sdr["IpCreate"];
                            if (sdr["NoteInterne"] != DBNull.Value) fattura.NoteInterne = (string)sdr["NoteInterne"];
                            if (sdr["StatoMittBreve"] != DBNull.Value) fattura.StatoMittBreve = (string)sdr["StatoMittBreve"];
                            if (sdr["StatoMittLungo"] != DBNull.Value) fattura.StatoMittLungo = (string)sdr["StatoMittLungo"];
                            if (sdr["idLotto"] != DBNull.Value) fattura.idLotto = (long)sdr["idLotto"];

                            if (fattura.idLotto > 0)
                            {
                                if (!lotti.Exists(x => x.Id == fattura.idLotto))
                                {
                                    cmd = new SqlCommand("proc_sel_Lotto", con);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Transaction = myTran;
                                    cmd.Parameters.Add(new SqlParameter("@idUser", userId));
                                    cmd.Parameters.Add(new SqlParameter("@idLotto", fattura.idLotto));
                                    using (SqlDataReader rdr = cmd.ExecuteReader())
                                    {
                                        if (rdr.Read())
                                        {
                                            Lotto lotto = new Lotto();
                                            lotto.Fatture = new List<FatturaModel>();
                                            if (sdr["id"] != DBNull.Value) lotto.Id = (long)rdr["id"];
                                            if (sdr["idUser"] != DBNull.Value) lotto.IdUser = (long)rdr["idUser"];
                                            if (sdr["NomeFile"] != DBNull.Value) lotto.NomeFile = (string)rdr["NomeFile"];
                                            if (sdr["ProgressivoInvio"] != DBNull.Value) lotto.ProgressivoInvio = (string)rdr["ProgressivoInvio"];
                                            if (sdr["Denominazione"] != DBNull.Value) lotto.Denominazione = (string)rdr["Denominazione"];
                                            if (sdr["IdFiscaleIVA"] != DBNull.Value) lotto.IdFiscaleIva = (string)rdr["IdFiscaleIVA"];
                                            if (sdr["ImportoTotaleDocumento"] != DBNull.Value) lotto.ImportoTotaleDocumento = (decimal)rdr["ImportoTotaleDocumento"];
                                            if (sdr["FormatoTrasmissione"] != DBNull.Value) lotto.FormatoTrasmissione = (string)rdr["FormatoTrasmissione"];
                                            if (sdr["NomeFileMetadati"] != DBNull.Value) lotto.NomeFileMetadati = (string)rdr["NomeFileMetadati"];
                                            if (sdr["idStato"] != DBNull.Value) lotto.IdStato = (short)rdr["idStato"];
                                            if (sdr["DataCreate"] != DBNull.Value) lotto.DataCreate = (DateTime)rdr["DataCreate"];
                                            if (sdr["NoteInterne"] != DBNull.Value) lotto.NoteInterne = (string)rdr["NoteInterne"];
                                            if (sdr["StatoMittBreve"] != DBNull.Value) lotto.StatoMittBreve = (string)rdr["StatoMittBreve"];
                                            if (sdr["StatoMittLungo"] != DBNull.Value) lotto.StatoMittLungo = (string)rdr["StatoMittLungo"];

                                            lotto.Fatture.Add(fattura);
                                            lotti.Add(lotto);
                                        }
                                    }
                                }
                                else
                                {
                                    var lotto = lotti.Find(x => x.Id == fattura.idLotto);
                                    lotto.Fatture.Add(fattura);
                                }
                            }
                            else
                            {
                                fatture.Add(fattura);
                            }
                        }
                    }
                    myTran.Commit();
                }

                foreach (Lotto lotto in lotti)
                {
                    var doc = new Documento();
                    doc.IsLotto = true;
                    doc.Lotto = lotto;
                    doc.Fattura = null;
                    documenti.Add(doc);
                }
                foreach (FatturaModel fat in fatture)
                {
                    var doc = new Documento();
                    doc.IsLotto = false;
                    doc.Lotto = null;
                    doc.Fattura = fat;
                    documenti.Add(doc);
                }
                return documenti;
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        LogWriter.WriteException(ex2);
                    }
                return null;
            }
        }

        public static List<FatturaModel> getFattureAttive(long userId, DateTime? dataIn = null, DateTime? dataOut = null)
        {
            List<FatturaModel> fatture = new List<FatturaModel>();
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_FattureAttive", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@idUser", userId));

                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        FatturaModel fattura = new FatturaModel();
                        if (sdr["id"] != DBNull.Value) fattura.Id = (long)sdr["id"];
                        if (sdr["idUser"] != DBNull.Value) fattura.IdUser = (long)sdr["idUser"];
                        if (sdr["NomeFile"] != DBNull.Value) fattura.NomeFile = (string)sdr["NomeFile"];
                        if (sdr["ProgressivoInvio"] != DBNull.Value) fattura.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
                        if (sdr["Denominazione"] != DBNull.Value) fattura.Denominazione = (string)sdr["Denominazione"];
                        if (sdr["IdFiscaleIVA"] != DBNull.Value) fattura.IdFiscaleIva = (string)sdr["IdFiscaleIVA"];
                        if (sdr["FormatoTrasmissione"] != DBNull.Value) fattura.FormatoTrasmissione = (string)sdr["FormatoTrasmissione"];
                        if (sdr["TipoDocumento"] != DBNull.Value) fattura.TipoDocumento = (string)sdr["TipoDocumento"];
                        if (sdr["NumeroFattura"] != DBNull.Value) fattura.NumeroFattura = (string)sdr["NumeroFattura"];
                        if (sdr["Data"] != DBNull.Value) fattura.Data = (DateTime)sdr["Data"];
                        if (sdr["ImportoTotaleDocumento"] != DBNull.Value) fattura.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
                        if (sdr["NomeFileMetadati"] != DBNull.Value) fattura.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
                        if (sdr["DataPagamento"] != DBNull.Value) fattura.DataPagamento = (DateTime)sdr["DataPagamento"];
                        if (sdr["idStato"] != DBNull.Value) fattura.IdStato = (short)sdr["idStato"];
                        if (sdr["IdentificativoSdI"] != DBNull.Value) fattura.IdentificativoSDI = (long)sdr["IdentificativoSdI"];
                        if (sdr["DataCreate"] != DBNull.Value) fattura.DataCreate = (DateTime)sdr["DataCreate"];
                        if (sdr["IpCreate"] != DBNull.Value) fattura.IpCreate = (string)sdr["IpCreate"];
                        if (sdr["NoteInterne"] != DBNull.Value) fattura.NoteInterne = (string)sdr["NoteInterne"];
                        if (sdr["StatoMittBreve"] != DBNull.Value) fattura.StatoMittBreve = (string)sdr["StatoMittBreve"];
                        if (sdr["StatoMittLungo"] != DBNull.Value) fattura.StatoMittLungo = (string)sdr["StatoMittLungo"];
                        if (sdr["idLotto"] != DBNull.Value) fattura.idLotto = (long)sdr["idLotto"];

                        fatture.Add(fattura);

                    }


                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");

                    return fatture;
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
            }
            return null;
        }

        public static List<Documento> getDocumentiPassivi(long userId, DateTime? dataIn = null, DateTime? dataOut = null)
        {
            SqlTransaction myTran = null;

            List<Documento> documenti = new List<Documento>();
            List<Lotto> lotti = new List<Lotto>();
            List<FatturaModel> fatture = new List<FatturaModel>();
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    myTran = con.BeginTransaction();
                    var cmd = new SqlCommand("proc_sel_FatturePassive", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Transaction = myTran;
                    cmd.Parameters.Add(new SqlParameter("@idUser", userId));
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            FatturaModel fattura = new FatturaModel();
                            if (sdr["id"] != DBNull.Value) fattura.Id = (long)sdr["id"];
                            if (sdr["idUser"] != DBNull.Value) fattura.IdUser = (long)sdr["idUser"];
                            if (sdr["NomeFile"] != DBNull.Value) fattura.NomeFile = (string)sdr["NomeFile"];
                            if (sdr["ProgressivoInvio"] != DBNull.Value) fattura.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
                            if (sdr["Denominazione"] != DBNull.Value) fattura.Denominazione = (string)sdr["Denominazione"];
                            if (sdr["IdFiscaleIVA"] != DBNull.Value) fattura.IdFiscaleIva = (string)sdr["IdFiscaleIVA"];
                            if (sdr["FormatoTrasmissione"] != DBNull.Value) fattura.FormatoTrasmissione = (string)sdr["FormatoTrasmissione"];
                            if (sdr["TipoDocumento"] != DBNull.Value) fattura.TipoDocumento = (string)sdr["TipoDocumento"];
                            if (sdr["NumeroFattura"] != DBNull.Value) fattura.NumeroFattura = (string)sdr["NumeroFattura"];
                            if (sdr["Data"] != DBNull.Value) fattura.Data = (DateTime)sdr["Data"];
                            if (sdr["ImportoTotaleDocumento"] != DBNull.Value) fattura.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
                            if (sdr["NomeFileMetadati"] != DBNull.Value) fattura.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
                            if (sdr["DataPagamento"] != DBNull.Value) fattura.DataPagamento = (DateTime)sdr["DataPagamento"];
                            if (sdr["idStato"] != DBNull.Value) fattura.IdStato = (short)sdr["idStato"];
                            if (sdr["IdentificativoSdI"] != DBNull.Value) fattura.IdentificativoSDI = (long)sdr["IdentificativoSdI"];
                            if (sdr["DataCreate"] != DBNull.Value) fattura.DataCreate = (DateTime)sdr["DataCreate"];
                            //if (sdr["IpCreate"] != DBNull.Value) fattura.IpCreate = (string)sdr["IpCreate"];
                            if (sdr["NoteInterne"] != DBNull.Value) fattura.NoteInterne = (string)sdr["NoteInterne"];
                            if (sdr["StatoDestBreve"] != DBNull.Value) fattura.StatoDestBreve = (string)sdr["StatoDestBreve"];
                            if (sdr["StatoDestLungo"] != DBNull.Value) fattura.StatoDestLungo = (string)sdr["StatoDestLungo"];
                            if (sdr["idLotto"] != DBNull.Value) fattura.idLotto = (long)sdr["idLotto"];

                            if (fattura.idLotto > 0)
                            {
                                if (!lotti.Exists(x => x.Id == fattura.idLotto))
                                {
                                    cmd = new SqlCommand("proc_sel_Lotto", con);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Transaction = myTran;
                                    cmd.Parameters.Add(new SqlParameter("@idUser", userId));
                                    cmd.Parameters.Add(new SqlParameter("@idLotto", fattura.idLotto));
                                    using (SqlDataReader rdr = cmd.ExecuteReader())
                                    {
                                        if (rdr.Read())
                                        {
                                            Lotto lotto = new Lotto();
                                            lotto.Fatture = new List<FatturaModel>();
                                            if (sdr["id"] != DBNull.Value) lotto.Id = (long)rdr["id"];
                                            if (sdr["idUser"] != DBNull.Value) lotto.IdUser = (long)rdr["idUser"];
                                            if (sdr["NomeFile"] != DBNull.Value) lotto.NomeFile = (string)rdr["NomeFile"];
                                            if (sdr["ProgressivoInvio"] != DBNull.Value) lotto.ProgressivoInvio = (string)rdr["ProgressivoInvio"];
                                            if (sdr["Denominazione"] != DBNull.Value) lotto.Denominazione = (string)rdr["Denominazione"];
                                            if (sdr["IdFiscaleIVA"] != DBNull.Value) lotto.IdFiscaleIva = (string)rdr["IdFiscaleIVA"];
                                            if (sdr["ImportoTotaleDocumento"] != DBNull.Value) lotto.ImportoTotaleDocumento = (decimal)rdr["ImportoTotaleDocumento"];
                                            if (sdr["FormatoTrasmissione"] != DBNull.Value) lotto.FormatoTrasmissione = (string)rdr["FormatoTrasmissione"];
                                            if (sdr["NomeFileMetadati"] != DBNull.Value) lotto.NomeFileMetadati = (string)rdr["NomeFileMetadati"];
                                            if (sdr["idStato"] != DBNull.Value) lotto.IdStato = (short)rdr["idStato"];
                                            if (sdr["DataCreate"] != DBNull.Value) lotto.DataCreate = (DateTime)rdr["DataCreate"];
                                            //if (sdr["NoteInterne"] != DBNull.Value) lotto.NoteInterne = (string)rdr["NoteInterne"];
                                            if (sdr["StatoDestBreve"] != DBNull.Value) lotto.StatoDestBreve = (string)rdr["StatoDestBreve"];
                                            if (sdr["StatoDestLungo"] != DBNull.Value) lotto.StatoDestLungo = (string)rdr["StatoDestLungo"];

                                            lotto.Fatture.Add(fattura);
                                            lotti.Add(lotto);
                                        }
                                    }
                                }
                                else
                                {
                                    var lotto = lotti.Find(x => x.Id == fattura.idLotto);
                                    lotto.Fatture.Add(fattura);
                                }
                            }
                            else
                            {
                                fatture.Add(fattura);
                            }
                        }
                    }
                    myTran.Commit();
                }

                foreach (Lotto lotto in lotti)
                {
                    var doc = new Documento();
                    doc.IsLotto = true;
                    doc.Lotto = lotto;
                    doc.Fattura = null;
                    documenti.Add(doc);
                }
                foreach (FatturaModel fat in fatture)
                {
                    var doc = new Documento();
                    doc.IsLotto = false;
                    doc.Lotto = null;
                    doc.Fattura = fat;
                    documenti.Add(doc);
                }
                return documenti;
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        LogWriter.WriteException(ex2);
                    }
                return null;
            }
        }

        public static string getUserPasswordHash(string username)
        {
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_UserPasswordHash", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@username", username));

                    SqlDataReader sdr = cmd.ExecuteReader();
                    if (sdr.Read())
                    {
                        if (sdr["Password"] != DBNull.Value) return (string)sdr["Password"];
                    }

                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");

                    return null;
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
            }
            return null;
        }

        public static string getPartnerPasswordHash(string Username)
        {
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_PartnerPasswordHash", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@Username", Username));

                    SqlDataReader sdr = cmd.ExecuteReader();
                    if (sdr.Read())
                    {
                        if (sdr["Password"] != DBNull.Value) return (string)sdr["Password"];
                    }

                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");

                    return null;
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
            }
            return null;
        }

        public static List<FatturaModel> getFatturePassive(long userId, DateTime? dataIn = null, DateTime? dataOut = null)
        {
            List<FatturaModel> fatture = new List<FatturaModel>();
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_FatturePassive", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@idUser", userId));

                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        FatturaModel fattura = new FatturaModel();
                        if (sdr["id"] != DBNull.Value) fattura.Id = (long)sdr["id"];
                        if (sdr["idUser"] != DBNull.Value) fattura.IdUser = (long)sdr["idUser"];
                        if (sdr["NomeFile"] != DBNull.Value) fattura.NomeFile = (string)sdr["NomeFile"];
                        if (sdr["ProgressivoInvio"] != DBNull.Value) fattura.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
                        if (sdr["Denominazione"] != DBNull.Value) fattura.Denominazione = (string)sdr["Denominazione"];
                        if (sdr["IdFiscaleIVA"] != DBNull.Value) fattura.IdFiscaleIva = (string)sdr["IdFiscaleIVA"];
                        if (sdr["FormatoTrasmissione"] != DBNull.Value) fattura.FormatoTrasmissione = (string)sdr["FormatoTrasmissione"];
                        if (sdr["TipoDocumento"] != DBNull.Value) fattura.TipoDocumento = (string)sdr["TipoDocumento"];
                        if (sdr["NumeroFattura"] != DBNull.Value) fattura.NumeroFattura = (string)sdr["NumeroFattura"];
                        if (sdr["Data"] != DBNull.Value) fattura.Data = (DateTime)sdr["Data"];
                        if (sdr["ImportoTotaleDocumento"] != DBNull.Value) fattura.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
                        if (sdr["NomeFileMetadati"] != DBNull.Value) fattura.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
                        if (sdr["DataPagamento"] != DBNull.Value) fattura.DataPagamento = (DateTime)sdr["DataPagamento"];
                        if (sdr["idStato"] != DBNull.Value) fattura.IdStato = (short)sdr["idStato"];
                        if (sdr["IdentificativoSdI"] != DBNull.Value) fattura.IdentificativoSDI = (long)sdr["IdentificativoSdI"];
                        if (sdr["DataCreate"] != DBNull.Value) fattura.DataCreate = (DateTime)sdr["DataCreate"];
                        //if (sdr["IpCreate"] != DBNull.Value) fattura.IpCreate = (string)sdr["IpCreate"];
                        if (sdr["NoteInterne"] != DBNull.Value) fattura.NoteInterne = (string)sdr["NoteInterne"];
                        if (sdr["StatoDestBreve"] != DBNull.Value) fattura.StatoDestBreve = (string)sdr["StatoDestBreve"];
                        if (sdr["StatoDestLungo"] != DBNull.Value) fattura.StatoDestLungo = (string)sdr["StatoDestLungo"];
                        if (sdr["idLotto"] != DBNull.Value) fattura.idLotto = (long)sdr["idLotto"];

                        fatture.Add(fattura);

                    }


                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");

                    return fatture;
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
            }
            return null;
        }

        public static FatturaModel getFatturaAttiva(string username, string pwd, long idFat)
        {
            try
            {
                FatturaModel fattura = null;
                User utente = new User(username, pwd);
                if (utente.id > 0)
                {
                    using (SqlConnection con = ConfManager.GetDbConnection())
                    {
                        con.Open();
                        var cmd = new SqlCommand("proc_sel_FatturaAttiva", con)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        cmd.Parameters.Add(new SqlParameter("@idFattura", idFat));
                        cmd.Parameters.Add(new SqlParameter("@idUser", utente.id));

                        SqlDataReader sdr = cmd.ExecuteReader();
                        while (sdr.Read())
                        {
                            fattura = new FatturaModel();
                            if (sdr["id"] != DBNull.Value) fattura.Id = (long)sdr["id"];
                            if (sdr["idUser"] != DBNull.Value) fattura.IdUser = (long)sdr["idUser"];
                            if (sdr["NomeFile"] != DBNull.Value) fattura.NomeFile = (string)sdr["NomeFile"];
                            if (sdr["ProgressivoInvio"] != DBNull.Value)
                                fattura.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
                            if (sdr["Denominazione"] != DBNull.Value)
                                fattura.Denominazione = (string)sdr["Denominazione"];
                            if (sdr["IdFiscaleIVA"] != DBNull.Value)
                                fattura.IdFiscaleIva = (string)sdr["IdFiscaleIVA"];
                            if (sdr["FormatoTrasmissione"] != DBNull.Value)
                                fattura.FormatoTrasmissione = (string)sdr["FormatoTrasmissione"];
                            if (sdr["TipoDocumento"] != DBNull.Value)
                                fattura.TipoDocumento = (string)sdr["TipoDocumento"];
                            if (sdr["NumeroFattura"] != DBNull.Value)
                                fattura.NumeroFattura = (string)sdr["NumeroFattura"];
                            if (sdr["Data"] != DBNull.Value) fattura.Data = (DateTime)sdr["Data"];
                            if (sdr["ImportoTotaleDocumento"] != DBNull.Value)
                                fattura.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
                            if (sdr["NomeFileMetadati"] != DBNull.Value)
                                fattura.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
                            if (sdr["DataPagamento"] != DBNull.Value)
                                fattura.DataPagamento = (DateTime)sdr["DataPagamento"];
                            if (sdr["idStato"] != DBNull.Value) fattura.IdStato = (short)sdr["idStato"];
                            if (sdr["IdentificativoSdI"] != DBNull.Value)
                                fattura.IdentificativoSDI = (long)sdr["IdentificativoSdI"];
                            if (sdr["DataCreate"] != DBNull.Value) fattura.DataCreate = (DateTime)sdr["DataCreate"];
                            //if (sdr["IpCreate"] != DBNull.Value) fattura.IpCreate = (string)sdr["IpCreate"];
                            if (sdr["NoteInterne"] != DBNull.Value) fattura.NoteInterne = (string)sdr["NoteInterne"];
                        }
                    }
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                    return fattura;
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
            }
            return null;
        }

        public static FatturaModel getFatturaPassiva(string username, string pwd, long idFat)
        {
            try
            {
                FatturaModel fattura = null;
                User utente = new User(username, pwd);
                if (utente.id > 0)
                {
                    using (SqlConnection con = ConfManager.GetDbConnection())
                    {
                        con.Open();
                        var cmd = new SqlCommand("proc_sel_FatturaPassiva", con)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        cmd.Parameters.Add(new SqlParameter("@idFattura", idFat));
                        cmd.Parameters.Add(new SqlParameter("@idUser", utente.id));

                        SqlDataReader sdr = cmd.ExecuteReader();
                        while (sdr.Read())
                        {
                            fattura = new FatturaModel();
                            if (sdr["id"] != DBNull.Value) fattura.Id = (long)sdr["id"];
                            if (sdr["idUser"] != DBNull.Value) fattura.IdUser = (long)sdr["idUser"];
                            if (sdr["NomeFile"] != DBNull.Value) fattura.NomeFile = (string)sdr["NomeFile"];
                            if (sdr["ProgressivoInvio"] != DBNull.Value)
                                fattura.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
                            if (sdr["Denominazione"] != DBNull.Value)
                                fattura.Denominazione = (string)sdr["Denominazione"];
                            if (sdr["IdFiscaleIVA"] != DBNull.Value)
                                fattura.IdFiscaleIva = (string)sdr["IdFiscaleIVA"];
                            if (sdr["FormatoTrasmissione"] != DBNull.Value)
                                fattura.FormatoTrasmissione = (string)sdr["FormatoTrasmissione"];
                            if (sdr["FormatoTrasmissione"] != DBNull.Value)
                                fattura.TipoDocumento = (string)sdr["TipoDocumento"];
                            if (sdr["NumeroFattura"] != DBNull.Value)
                                fattura.NumeroFattura = (string)sdr["NumeroFattura"];
                            if (sdr["Data"] != DBNull.Value) fattura.Data = (DateTime)sdr["Data"];
                            if (sdr["ImportoTotaleDocumento"] != DBNull.Value)
                                fattura.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
                            if (sdr["NomeFileMetadati"] != DBNull.Value)
                                fattura.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
                            if (sdr["DataPagamento"] != DBNull.Value)
                                fattura.DataPagamento = (DateTime)sdr["DataPagamento"];
                            if (sdr["idStato"] != DBNull.Value) fattura.IdStato = (short)sdr["idStato"];
                            if (sdr["IdentificativoSdI"] != DBNull.Value)
                                fattura.IdentificativoSDI = (long)sdr["IdentificativoSdI"];
                            if (sdr["DataCreate"] != DBNull.Value) fattura.DataCreate = (DateTime)sdr["DataCreate"];
                            //if (sdr["IpCreate"] != DBNull.Value) fattura.IpCreate = (string)sdr["IpCreate"];
                            if (sdr["NoteInterne"] != DBNull.Value) fattura.NoteInterne = (string)sdr["NoteInterne"];
                        }
                    }
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                    return fattura;
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
            }
            return null;
        }

        public static string getXmlFileFatturaAttiva(string username, string pwd, long idFattura)
        {
            try
            {
                FatturaModel fattura = getFatturaAttiva(username, pwd, idFattura);
                if (fattura != null)
                {
                    string fileName = Path.Combine(DbQueries.GetConfigField(Const.DB_FILES_PATH_FIELD), Convert.ToString(fattura.IdUser),
                        Const.FATTURE_ATTIVE_FOLDER, fattura.NomeFile);
                    return fileName;
                    //return File.ReadAllText(fileName);
                }
                return "";
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string getXmlFileFatturaPassiva(string username, string pwd, long idFattura)
        {
            try
            {
                FatturaModel fattura = getFatturaPassiva(username, pwd, idFattura);
                if (fattura != null)
                {
                    string fileName = Path.Combine(DbQueries.GetConfigField(Const.DB_FILES_PATH_FIELD), Convert.ToString(fattura.IdUser),
                        Const.FATTURE_PASSIVE_FOLDER, fattura.NomeFile);
                    return fileName;
                    //return File.ReadAllText(fileName);
                }
                return "";
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static long insLottoFatture(string username, string pwd, Fattura fattura, string fileName, string FormatoTrasmissione)
        {
            SqlTransaction myTran = null;
            try
            {
                User utente = checkUser(username, pwd);
                if (utente != null)
                {
                    long idFat = 0;
                    long idLotto = 0;
                    using (SqlConnection con = ConfManager.GetDbConnection())
                    {
                        con.Open();
                        myTran = con.BeginTransaction();

                        SqlCommand command = new SqlCommand("proc_ins_Lotto", con);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Transaction = myTran;
                        command.Parameters.AddWithValue("idUser", utente.id);
                        command.Parameters.AddWithValue("NomeFile", fileName);
                        command.Parameters.AddWithValue("ProgressivoInvio",
                                                        fattura.FatturaElettronicaHeader.DatiTrasmissione.ProgressivoInvio);
                        if (fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici.Anagrafica.Denominazione == null)
                            command.Parameters.AddWithValue("Denominazione",
                                                            fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                                   .Anagrafica.CognomeNome);
                        else command.Parameters.AddWithValue("Denominazione",
                                                        fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                               .Anagrafica.Denominazione);
                        command.Parameters.AddWithValue("idStato", 1);
                        command.Parameters.AddWithValue("IdFiscaleIVA",
                                                        fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                               .IdFiscaleIVA.IdCodice +
                                                        fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                               .IdFiscaleIVA.IdPaese);
                        command.Parameters.AddWithValue("FormatoTrasmissione", FormatoTrasmissione);
                        command.Parameters.AddWithValue("DataCreate", DateTime.Now);


                        command.Parameters.Add("@retValue", System.Data.SqlDbType.BigInt).Direction =
                                    ParameterDirection.ReturnValue;
                        command.ExecuteNonQuery();
                        idLotto = Convert.ToInt64(command.Parameters["@retValue"].Value);


                        foreach (var body in fattura.FatturaElettronicaBody)
                        {
                            var numeroFat = body.DatiGenerali.DatiGeneraliDocumento.Numero;
                            var dataFat = body.DatiGenerali.DatiGeneraliDocumento.Data;

                            command = new SqlCommand("proc_sel_FattureAttive", con);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Transaction = myTran;
                            command.Parameters.AddWithValue("NumeroFattura", numeroFat);
                            command.Parameters.AddWithValue("DataFattura", dataFat);
                            command.Parameters.AddWithValue("idUser", utente.id);
                            using (SqlDataReader rdr = command.ExecuteReader())
                            {
                                if (rdr != null && rdr.Read())
                                {
                                    command = new SqlCommand("proc_ins_FatturaAttiva", con);
                                    command.CommandType = CommandType.StoredProcedure;
                                    command.Transaction = myTran;
                                    command.Parameters.AddWithValue("idUser", utente.id);
                                    command.Parameters.AddWithValue("NomeFile", fileName);
                                    command.Parameters.AddWithValue("ProgressivoInvio",
                                                                    fattura.FatturaElettronicaHeader.DatiTrasmissione.ProgressivoInvio);
                                    if (fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici.Anagrafica.Denominazione == null)
                                        command.Parameters.AddWithValue("Denominazione",
                                                                        fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                                               .Anagrafica.CognomeNome);
                                    else command.Parameters.AddWithValue("Denominazione",
                                                                    fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                                           .Anagrafica.Denominazione);
                                    command.Parameters.AddWithValue("IdFiscaleIVA",
                                                                    fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                                           .IdFiscaleIVA.IdCodice +
                                                                    fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                                           .IdFiscaleIVA.IdPaese);
                                    command.Parameters.AddWithValue("FormatoTrasmissione", FormatoTrasmissione);
                                    command.Parameters.AddWithValue("TipoDocumento",
                                                                    body.DatiGenerali.DatiGeneraliDocumento
                                                                        .TipoDocumento);
                                    command.Parameters.AddWithValue("NumeroFattura",
                                                                    body.DatiGenerali.DatiGeneraliDocumento.Numero);
                                    command.Parameters.AddWithValue("Data", body.DatiGenerali.DatiGeneraliDocumento.Data);
                                    decimal importo = 0;
                                    foreach (var dettaglio in fattura.FatturaElettronicaBody[0].DatiBeniServizi.DatiRiepilogo)
                                    {
                                        importo += (dettaglio.ImponibileImporto + dettaglio.Imposta);
                                    }
                                    command.Parameters.AddWithValue("ImportoTotaleDocumento", importo);

                                    command.Parameters.AddWithValue("idStato", 1);
                                    command.Parameters.AddWithValue("DataCreate", DateTime.Now);
                                    command.Parameters.AddWithValue("IpCreate", MyUtils.GetIPv4Address());
                                    command.Parameters.AddWithValue("idLotto", idLotto);
                                    command.Parameters.Add("@retValue", System.Data.SqlDbType.BigInt).Direction =
                                        ParameterDirection.ReturnValue;
                                    command.ExecuteNonQuery();
                                    idFat = Convert.ToInt64(command.Parameters["@retValue"].Value);

                                    foreach (var riga in body.DatiBeniServizi.DettaglioLinee)
                                    {
                                        command = new SqlCommand("proc_ins_FatturaRigAttiva", con);
                                        command.CommandType = CommandType.StoredProcedure;
                                        command.Transaction = myTran;
                                        command.Parameters.AddWithValue("idFatt", idFat);
                                        command.Parameters.AddWithValue("Descrizione", riga.Descrizione);
                                        command.Parameters.AddWithValue("Quantita",
                                                                        riga.Quantita);
                                        command.Parameters.AddWithValue("Importo", riga.PrezzoTotale);
                                        command.ExecuteNonQuery();
                                    }
                                }
                                else
                                {
                                    return -1;
                                }
                            }
                        }
                        myTran.Commit();
                    }
                    return idFat;
                }
                else
                {
                    return -1;
                }

            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        LogWriter.WriteException(ex2);
                    }
                return -1;
            }
        }

        public static long insFatturaSingola(string username, string pwd, Fattura fattura, string fileName, string FormatoTrasmissione)
        {
            SqlTransaction myTran = null;
            try
            {
                User utente = checkUser(username, pwd);
                if (utente != null)
                {
                    long idFat = 0;
                    using (SqlConnection con = ConfManager.GetDbConnection())
                    {
                        con.Open();
                        myTran = con.BeginTransaction();

                        var numeroFat = fattura.FatturaElettronicaBody[0].DatiGenerali.DatiGeneraliDocumento.Numero;
                        var dataFat = fattura.FatturaElettronicaBody[0].DatiGenerali.DatiGeneraliDocumento.Data;

                        SqlCommand command = new SqlCommand("proc_sel_FattureAttive", con);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Transaction = myTran;
                        command.Parameters.AddWithValue("NumeroFattura", numeroFat);
                        command.Parameters.AddWithValue("DataFattura", dataFat);
                        command.Parameters.AddWithValue("idUser", utente.id);
                        using (SqlDataReader rdr = command.ExecuteReader())
                        {
                            if (rdr != null && rdr.Read())
                            {
                                //la fattura esiste già
                                return -1;
                            }
                            else
                            {
                                command = new SqlCommand("proc_ins_FatturaAttiva", con);
                                command.CommandType = CommandType.StoredProcedure;
                                command.Transaction = myTran;
                                command.Parameters.AddWithValue("idUser", utente.id);
                                command.Parameters.AddWithValue("NomeFile", fileName);
                                command.Parameters.AddWithValue("ProgressivoInvio",
                                                                fattura.FatturaElettronicaHeader.DatiTrasmissione.ProgressivoInvio);
                                if (fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici.Anagrafica.Denominazione == null)
                                    command.Parameters.AddWithValue("Denominazione",
                                                                    fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                                           .Anagrafica.CognomeNome);
                                else command.Parameters.AddWithValue("Denominazione",
                                                                fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                                       .Anagrafica.Denominazione);
                                command.Parameters.AddWithValue("IdFiscaleIVA",
                                                                fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                                       .IdFiscaleIVA.IdCodice +
                                                                fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici
                                                                       .IdFiscaleIVA.IdPaese);
                                command.Parameters.AddWithValue("FormatoTrasmissione", FormatoTrasmissione);
                                command.Parameters.AddWithValue("TipoDocumento",
                                                                fattura.FatturaElettronicaBody[0].DatiGenerali.DatiGeneraliDocumento
                                                                    .TipoDocumento);
                                command.Parameters.AddWithValue("NumeroFattura",
                                                                fattura.FatturaElettronicaBody[0].DatiGenerali.DatiGeneraliDocumento.Numero);
                                command.Parameters.AddWithValue("Data", fattura.FatturaElettronicaBody[0].DatiGenerali.DatiGeneraliDocumento.Data);
                                //if (fattura.FatturaElettronicaBody[0].DatiPagamento.Count > 0 &&
                                //    fattura.FatturaElettronicaBody[0].DatiPagamento[0].DettaglioPagamento.Count > 0)
                                //    command.Parameters.AddWithValue("ImportoTotaleDocumento",
                                //                                    fattura.FatturaElettronicaBody[0].DatiPagamento[0].DettaglioPagamento[0]
                                //                                        .ImportoPagamento);
                                //else
                                decimal importo = 0;
                                foreach (var dettaglio in fattura.FatturaElettronicaBody[0].DatiBeniServizi.DatiRiepilogo)
                                {
                                    importo += (dettaglio.ImponibileImporto + dettaglio.Imposta);
                                }
                                command.Parameters.AddWithValue("ImportoTotaleDocumento", importo);

                                command.Parameters.AddWithValue("idStato", 1);
                                command.Parameters.AddWithValue("DataCreate", DateTime.Now);
                                command.Parameters.AddWithValue("IpCreate", MyUtils.GetIPv4Address());
                                command.Parameters.AddWithValue("idLotto", 0);
                                command.Parameters.Add("@retValue", System.Data.SqlDbType.BigInt).Direction =
                                    ParameterDirection.ReturnValue;
                                command.ExecuteNonQuery();
                                idFat = Convert.ToInt64(command.Parameters["@retValue"].Value);

                                foreach (var riga in fattura.FatturaElettronicaBody[0].DatiBeniServizi.DettaglioLinee)
                                {
                                    command = new SqlCommand("proc_ins_FatturaRigAttiva", con);
                                    command.CommandType = CommandType.StoredProcedure;
                                    command.Transaction = myTran;
                                    command.Parameters.AddWithValue("idFatt", idFat);
                                    command.Parameters.AddWithValue("Descrizione", riga.Descrizione);
                                    command.Parameters.AddWithValue("Quantita",
                                                                    riga.Quantita);
                                    command.Parameters.AddWithValue("Importo", riga.PrezzoTotale);
                                    command.ExecuteNonQuery();
                                }
                            }
                        }

                        myTran.Commit();
                    }
                    return idFat;
                }
                else
                {
                    return -1;
                }

            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        LogWriter.WriteException(ex2);
                    }
                return -1;
            }
        }

        public static List<MessaggioSDI> getMessaggiSDI(string username, string pwd)
        {
            
            List<MessaggioSDI> messaggi = new List<MessaggioSDI>();
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_MessaggiSDI", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@UserName", username));
                    cmd.Parameters.Add(new SqlParameter("@Password", pwd));

                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        MessaggioSDI messaggio = new MessaggioSDI();
                        if (sdr["id"] != DBNull.Value) messaggio.id = (long)sdr["id"];
                        if (sdr["IdentificativoSDI"] != DBNull.Value) messaggio.IdentificativoSDI = (long)sdr["IdentificativoSDI"];
                        if (sdr["tipoFatt"] != DBNull.Value) messaggio.tipoFatt = (short)sdr["tipoFatt"];
                        if (sdr["idTipoMessaggio"] != DBNull.Value) messaggio.idTipoMessaggio = (short)sdr["idTipoMessaggio"];
                        if (sdr["NomeFile"] != DBNull.Value) messaggio.NomeFileNotifica = (string)sdr["NomeFile"];
                        if (sdr["FileFattura"] != DBNull.Value) messaggio.NomeFileFattura = (string)sdr["FileFattura"];
                        if (sdr["DataRicezione"] != DBNull.Value) messaggio.DataRicezione = (DateTime)sdr["DataRicezione"];
                        if (sdr["Titolo"] != DBNull.Value) messaggio.Titolo = (string)sdr["Titolo"];
                        if (sdr["Descrizione"] != DBNull.Value) messaggio.Descrizione = (string)sdr["Descrizione"];
                        if (sdr["Note"] != DBNull.Value) messaggio.Note = (string)sdr["Note"];
                        if (sdr["Letto"] != DBNull.Value) messaggio.Letto = (bool)sdr["Letto"];

                        messaggio.Errori = getErroriMessaggioSdi(messaggio.id);

                        messaggi.Add(messaggio);
                    }


                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");

                    return messaggi;
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
            }
            return null;

        }

        public static List<ErroriSDI> getErroriMessaggioSdi(long idMessaggio)
        {

            List<ErroriSDI> errori = new List<ErroriSDI>();
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_ErroriMessaggiSDI", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@idMessaggio", idMessaggio));

                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        ErroriSDI errore = new ErroriSDI();
                        if (sdr["Codice"] != DBNull.Value) errore.Codice = (string)sdr["Codice"];
                        if (sdr["Suggerimento"] != DBNull.Value) errore.Suggerimento = (string)sdr["Suggerimento"];
                        if (sdr["Descrizione"] != DBNull.Value) errore.Descrizione = (string)sdr["Descrizione"];

                        errori.Add(errore);
                    }


                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");

                    return errori;
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
            }
            return null;

        }

        public static TipoMessaggioSDI getTipoMessaggioSDI(short id)
        {
            
            TipoMessaggioSDI tipo = null;
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_MessaggioSDIDaTipo", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@id", id));

                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        tipo = new TipoMessaggioSDI();
                        tipo.id = id;
                        if (sdr["Descrizione"] != DBNull.Value) tipo.Descrizione = (string)sdr["Descrizione"];
                        if (sdr["Titolo"] != DBNull.Value) tipo.Titolo = (string)sdr["Titolo"];
                    }


                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");

                    return tipo;
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
            }
            return null;

        }

        public static List<Notifiche> getNotifiche(string username, string pwd)
        {
            //
            //List<MessaggioSDI> messaggi = new List<MessaggioSDI>();
            //try
            //{
            //    using (con = ConfManager.GetDbConnection())
            //    {
            //        con.Open();
            //        var cmd = new SqlCommand("proc_sel_Notifiche", con)
            //        {
            //            CommandType = CommandType.StoredProcedure
            //        };
            //        cmd.Parameters.Add(new SqlParameter("@UserName", username));
            //        cmd.Parameters.Add(new SqlParameter("@Password", pwd));

            //        SqlDataReader sdr = cmd.ExecuteReader();
            //        while (sdr.Read())
            //        {
            //            MessaggioSDI messaggio = new MessaggioSDI();
            //            if (sdr["id"] != DBNull.Value) fattura.Id = (long)sdr["id"];
            //            if (sdr["idUser"] != DBNull.Value) fattura.IdUser = (long)sdr["idUser"];
            //            if (sdr["NomeFile"] != DBNull.Value) fattura.NomeFile = (string)sdr["NomeFile"];
            //            if (sdr["ProgressivoInvio"] != DBNull.Value) fattura.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
            //            if (sdr["DenominazioneCessionario"] != DBNull.Value) fattura.DenominazioneCessionario = (string)sdr["DenominazioneCessionario"];
            //            if (sdr["IdFiscaleIVACessionario"] != DBNull.Value) fattura.IdFiscaleIvaCessionario = (string)sdr["IdFiscaleIVACessionario"];
            //            if (sdr["TipoDocumento"] != DBNull.Value) fattura.TipoDocumento = (string)sdr["TipoDocumento"];
            //            if (sdr["NumeroFattura"] != DBNull.Value) fattura.NumeroFattura = (string)sdr["NumeroFattura"];
            //            if (sdr["Data"] != DBNull.Value) fattura.Data = (DateTime)sdr["Data"];
            //            if (sdr["ImportoTotaleDocumento"] != DBNull.Value) fattura.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
            //            if (sdr["NomeFileMetadati"] != DBNull.Value) fattura.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
            //            if (sdr["DataPagamento"] != DBNull.Value) fattura.DataPagamento = (DateTime)sdr["DataPagamento"];
            //            if (sdr["idStato"] != DBNull.Value) fattura.IdStato = (short)sdr["idStato"];
            //            if (sdr["IdentificativoSdI"] != DBNull.Value) fattura.IdentificativoSDI = (long)sdr["IdentificativoSdI"];
            //            if (sdr["DataCreate"] != DBNull.Value) fattura.DataCreate = (DateTime)sdr["DataCreate"];
            //            //if (sdr["IpCreate"] != DBNull.Value) fattura.IpCreate = (string)sdr["IpCreate"];
            //            if (sdr["NoteInterne"] != DBNull.Value) fattura.NoteInterne = (string)sdr["NoteInterne"];

            //            fatture.Add(fattura);

            //        }


            //        //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");

            //        return fatture;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LogWriter.WriteException(ex);
            //}
            //finally
            //{
            //    if (con != null) con.Close();
            //}
            return null;

        }



        public static bool CambiaStatoFattura(long idFattura, long idUser, short stato)
        {
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@id", idFattura));
                    cmd.Parameters.Add(new SqlParameter("@idUser", idUser));
                    cmd.Parameters.Add(new SqlParameter("@IdStato", stato));

                    cmd.ExecuteNonQuery();
                    return true;
                }
                //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                return false;
            }
        }

        public static long NumeroMessaggiDaLeggere(string username, string pwd)
        {
            
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_NumeroMessaggiDaLeggere", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@UserName", username));
                    cmd.Parameters.Add(new SqlParameter("@Password", pwd));
                    cmd.Parameters.Add("@retValue", System.Data.SqlDbType.BigInt).Direction =
                                    ParameterDirection.ReturnValue;
                    cmd.ExecuteNonQuery();
                    return Convert.ToInt64(cmd.Parameters["@retValue"].Value);
                }
                //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                return -1;
            }
        }

        public static Dictionary<string, int> BadgeDocumentiAttivi(long userId, RequestDataInterval request)
        {
            try
            {
                Dictionary<string, int> response = new Dictionary<string, int>();
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_BadgeDocumentiAttivi", con)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                    cmd.Parameters.Add(new SqlParameter("@idUser", userId));
                    cmd.Parameters.Add(new SqlParameter("@StartDate", request.StartDate));
                    cmd.Parameters.Add(new SqlParameter("@EndDate", request.EndDate));


                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        if (sdr["idStato"] != DBNull.Value && sdr["numero"] != DBNull.Value)
                            response.Add("s" + Convert.ToString((short)sdr["idStato"]), (int)sdr["numero"]);
                    }

                    return response;
                }

                //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                return null;
            }
        }

        public static long MarcaMessaggioLetto(string username, string pwd, long idMessaggio)
        {
            
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_upd_MarcaMessaggioLetto", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter retValParam = new SqlParameter("@RETURN_VALUE", SqlDbType.BigInt);
                    retValParam.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(retValParam);
                    cmd.Parameters.Add(new SqlParameter("@UserName", username));
                    cmd.Parameters.Add(new SqlParameter("@Password", pwd));
                    cmd.Parameters.Add(new SqlParameter("@idMessaggio", idMessaggio));

                    cmd.ExecuteNonQuery();
                    return Convert.ToInt64(cmd.Parameters["@RETURN_VALUE"].Value);
                }
                //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                return -1;
            }
        }

        public static long MarcaTuttiMessaggiLetti(string username, string pwd)
        {
            
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_upd_MarcaTuttiMessaggiLetti", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter retValParam = new SqlParameter("@RETURN_VALUE", SqlDbType.BigInt);
                    retValParam.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(retValParam);
                    cmd.Parameters.Add(new SqlParameter("@UserName", username));
                    cmd.Parameters.Add(new SqlParameter("@Password", pwd));

                    cmd.ExecuteNonQuery();
                    return Convert.ToInt64(cmd.Parameters["@RETURN_VALUE"].Value);
                }
                //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                return -1;
            }
        }

        public static Lotto getLotto(string username, string pwd, long idLotto)
        {
            
            SqlTransaction myTran = null;
            Lotto lotto = null;
            try
            {
                User utente = new User(username, pwd);
                if (utente.id > 0)
                    using (SqlConnection con = ConfManager.GetDbConnection())
                    {
                        con.Open();
                        myTran = con.BeginTransaction();
                        var cmd = new SqlCommand("proc_sel_Lotto", con)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        cmd.Transaction = myTran;
                        cmd.Parameters.Add(new SqlParameter("@idUser", utente.id));
                        cmd.Parameters.Add(new SqlParameter("@idLotto", idLotto));

                        SqlDataReader sdr = cmd.ExecuteReader();
                        if (sdr.Read())
                        {
                            lotto = new Lotto();
                            lotto.Fatture = new List<FatturaModel>();
                            if (sdr["id"] != DBNull.Value) lotto.Id = (long)sdr["id"];
                            if (sdr["idUser"] != DBNull.Value) lotto.IdUser = (long)sdr["idUser"];
                            if (sdr["NomeFile"] != DBNull.Value) lotto.NomeFile = (string)sdr["NomeFile"];
                            if (sdr["ProgressivoInvio"] != DBNull.Value) lotto.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
                            if (sdr["FormatoTrasmissione"] != DBNull.Value) lotto.FormatoTrasmissione = (string)sdr["FormatoTrasmissione"];
                            if (sdr["Denominazione"] != DBNull.Value) lotto.Denominazione = (string)sdr["Denominazione"];
                            if (sdr["IdFiscaleIVA"] != DBNull.Value) lotto.IdFiscaleIva = (string)sdr["IdFiscaleIVA"];
                            if (sdr["ImportoTotaleDocumento"] != DBNull.Value) lotto.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
                            if (sdr["NomeFileMetadati"] != DBNull.Value) lotto.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
                            if (sdr["idStato"] != DBNull.Value) lotto.IdStato = (short)sdr["idStato"];
                            if (sdr["DataCreate"] != DBNull.Value) lotto.DataCreate = (DateTime)sdr["DataCreate"];
                            if (sdr["NoteInterne"] != DBNull.Value) lotto.NoteInterne = (string)sdr["NoteInterne"];
                        }

                        cmd = new SqlCommand("proc_sel_FattureAttDaLotto", con)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        cmd.Transaction = myTran;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@idLotto", idLotto));
                        cmd.Parameters.Add(new SqlParameter("@idUser", utente.id));

                        sdr = cmd.ExecuteReader();

                        while (sdr.Read())
                        {
                            if (sdr["id"] != DBNull.Value)
                            {
                                lotto.Fatture.Add(getFatturaAttiva(username, pwd, (long)sdr["id"]));
                            }
                        }

                        //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                        myTran.Commit();
                        return lotto;
                    }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        LogWriter.WriteException(ex2);
                    }
                return lotto;
            }
            return lotto;
        }

        public static bool CambiaStatoLottoFatture(long idLotto, long idUser, short stato)
        {
            
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_upd_StatoLottoFattureAttive", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@idLotto", idLotto));
                    cmd.Parameters.Add(new SqlParameter("@idUser", idUser));
                    cmd.Parameters.Add(new SqlParameter("@IdStato", stato));

                    cmd.ExecuteNonQuery();
                    return true;
                }
                //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                return false;
            }
        }

        public static User RegisterUser(User utente)
        {
            
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_ins_User", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@stato", utente.stato));
                    cmd.Parameters.Add(new SqlParameter("@idPartner", utente.idPartner));
                    cmd.Parameters.Add(new SqlParameter("@idReseller", utente.idReseller));
                    //cmd.Parameters.Add(new SqlParameter("@CreateDate", utente.CreateDate));
                    cmd.Parameters.Add(new SqlParameter("@Username", utente.Username));
                    cmd.Parameters.Add(new SqlParameter("@Password", utente.Password));
                    cmd.Parameters.Add(new SqlParameter("@Pin", utente.Pin));
                    cmd.Parameters.Add(new SqlParameter("@Logo", utente.Logo));
                    cmd.Parameters.Add(new SqlParameter("@Name", utente.Name));
                    cmd.Parameters.Add(new SqlParameter("@Surname", utente.Surname));
                    cmd.Parameters.Add(new SqlParameter("@Denominazione", utente.Denominazione));
                    cmd.Parameters.Add(new SqlParameter("@idPianoCommerciale", utente.idPianoCommerciale));
                    cmd.Parameters.Add(new SqlParameter("@IdFiscaleIVA", utente.IdFiscaleIVA));
                    cmd.Parameters.Add(new SqlParameter("@Address", utente.Address));
                    cmd.Parameters.Add(new SqlParameter("@PostCode", utente.PostCode));
                    cmd.Parameters.Add(new SqlParameter("@City", utente.City));
                    cmd.Parameters.Add(new SqlParameter("@Country", utente.Country));
                    cmd.Parameters.Add(new SqlParameter("@Telefono", utente.Telefono));
                    cmd.Parameters.Add(new SqlParameter("@Email", utente.Email));
                    cmd.Parameters.Add(new SqlParameter("@Pec", utente.Pec));
                    cmd.Parameters.Add(new SqlParameter("@Note", utente.Note));
                    //cmd.Parameters.Add(new SqlParameter("@LastAccessDate", utente.LastAccessDate));
                    cmd.Parameters.Add(new SqlParameter("@Active", utente.Active));
                    //cmd.Parameters.Add(new SqlParameter("@QuotaFatture", utente.QuotaFatture));
                    //cmd.Parameters.Add(new SqlParameter("@TotFatture", utente.TotFatture));
                    cmd.Parameters.Add(new SqlParameter("@EasyReader", utente.EasyReader));
                    //cmd.Parameters.Add(new SqlParameter("@DataFineSottoscrizione", utente.DataFineSottoscrizione));
                    //cmd.Parameters.Add(new SqlParameter("@DataFineQuota", utente.DataFineQuota));
                    var sdr = cmd.ExecuteReader();

                    if (sdr.Read())
                    {
                        if (sdr["id"] != DBNull.Value) utente.id = (long)sdr["id"];
                        if (sdr["stato"] != DBNull.Value) utente.stato = (int)sdr["stato"];
                        if (sdr["idReseller"] != DBNull.Value) utente.idReseller = (int)sdr["idReseller"];
                        if (sdr["idPartner"] != DBNull.Value) utente.idPartner = (int)sdr["idPartner"];
                        if (sdr["CreateDate"] != DBNull.Value) utente.CreateDate = (DateTime)sdr["CreateDate"];
                        if (sdr["Username"] != DBNull.Value) utente.Username = (string)sdr["Username"];
                        if (sdr["Password"] != DBNull.Value) utente.Password = (string)sdr["Password"];
                        if (sdr["Pin"] != DBNull.Value) utente.Pin = (string)sdr["Pin"];
                        if (sdr["Logo"] != DBNull.Value) utente.Logo = (string)sdr["Logo"];
                        if (sdr["Name"] != DBNull.Value) utente.Name = (string)sdr["Name"];
                        if (sdr["Surname"] != DBNull.Value) utente.Surname = (string)sdr["Surname"];
                        if (sdr["Denominazione"] != DBNull.Value) utente.Denominazione = (string)sdr["Denominazione"];
                        if (sdr["idPianoCommerciale"] != DBNull.Value)
                            utente.idPianoCommerciale = (int)sdr["idPianoCommerciale"];
                        if (sdr["IdFiscaleIVA"] != DBNull.Value) utente.IdFiscaleIVA = (string)sdr["IdFiscaleIVA"];
                        if (sdr["Address"] != DBNull.Value) utente.Address = (string)sdr["Address"];
                        if (sdr["PostCode"] != DBNull.Value) utente.PostCode = (string)sdr["PostCode"];
                        if (sdr["City"] != DBNull.Value) utente.City = (string)sdr["City"];
                        if (sdr["Country"] != DBNull.Value) utente.Country = (string)sdr["Country"];
                        if (sdr["Telefono"] != DBNull.Value) utente.Telefono = (string)sdr["Telefono"];
                        if (sdr["Email"] != DBNull.Value) utente.Email = (string)sdr["Email"];
                        if (sdr["Pec"] != DBNull.Value) utente.Pec = (string)sdr["Pec"];
                        if (sdr["Note"] != DBNull.Value) utente.Note = (string)sdr["Note"];
                        //if (sdr["LastAccessDate"] != DBNull.Value) utente.LastAccessDate = (DateTime?) sdr["LastAccessDate"];
                        if (sdr["Active"] != DBNull.Value) utente.Active = (bool)sdr["Active"];
                        //if (sdr["QuotaFatture"] != DBNull.Value) utente.QuotaFatture = (int) sdr["QuotaFatture"];
                        //if (sdr["TotFatture"] != DBNull.Value) utente.TotFatture = (int) sdr["TotFatture"];
                        //if (sdr["EasyReader"] != DBNull.Value) utente.EasyReader = (bool) sdr["EasyReader"];
                        //if (sdr["DataFineSottoscrizione"] != DBNull.Value) utente.DataFineSottoscrizione = (DateTime?) sdr["DataFineSottoscrizione"];
                        //if (sdr["DataFineQuota"] != DBNull.Value) utente.DataFineQuota = (DateTime?) sdr["DataFineQuota"];

                        return utente;
                    }
                    else return null;

                }
                //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                return null;
            }
        }

        private static User checkUser(string username, string pwd)
        {
            var hash = DbQueries.getUserPasswordHash(username);
            if (hash == null) return null;
            if (SecurePasswordHasher.Verify(pwd, hash))
            {
                User user = new User(username, hash);
                if (user.id > 0)
                {
                    //user.Password = pwd;
                    return user;
                }
                return null;
            }
            return null;
        }

        public static bool editUser(User utente)
        {
            
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_upd_User", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@id", utente.id));
                    cmd.Parameters.Add(new SqlParameter("@stato", utente.stato));
                    cmd.Parameters.Add(new SqlParameter("@idPartner", utente.idPartner));
                    cmd.Parameters.Add(new SqlParameter("@idReseller", utente.idReseller));
                    cmd.Parameters.Add(new SqlParameter("@CreateDate", utente.CreateDate));
                    cmd.Parameters.Add(new SqlParameter("@Username", utente.Username));
                    cmd.Parameters.Add(new SqlParameter("@Password", utente.Password));
                    cmd.Parameters.Add(new SqlParameter("@Pin", utente.Pin));
                    cmd.Parameters.Add(new SqlParameter("@Logo", utente.Logo));
                    cmd.Parameters.Add(new SqlParameter("@Name", utente.Name));
                    cmd.Parameters.Add(new SqlParameter("@Surname", utente.Surname));
                    cmd.Parameters.Add(new SqlParameter("@Denominazione", utente.Denominazione));
                    cmd.Parameters.Add(new SqlParameter("@idPianoCommerciale", utente.idPianoCommerciale));
                    cmd.Parameters.Add(new SqlParameter("@IdFiscaleIVA", utente.IdFiscaleIVA));
                    cmd.Parameters.Add(new SqlParameter("@Address", utente.Address));
                    cmd.Parameters.Add(new SqlParameter("@PostCode", utente.PostCode));
                    cmd.Parameters.Add(new SqlParameter("@City", utente.City));
                    cmd.Parameters.Add(new SqlParameter("@Country", utente.Country));
                    cmd.Parameters.Add(new SqlParameter("@Telefono", utente.Telefono));
                    cmd.Parameters.Add(new SqlParameter("@Email", utente.Email));
                    cmd.Parameters.Add(new SqlParameter("@Pec", utente.Pec));
                    cmd.Parameters.Add(new SqlParameter("@Note", utente.Note));
                    cmd.Parameters.Add(new SqlParameter("@Active", utente.Active));
                    cmd.Parameters.Add(new SqlParameter("@QuotaFatture", utente.QuotaFatture));
                    cmd.Parameters.Add(new SqlParameter("@TotFatture", utente.TotFatture));
                    cmd.Parameters.Add(new SqlParameter("@EasyReader", utente.EasyReader));
                    cmd.Parameters.Add(new SqlParameter("@DataFineSottoscrizione", utente.DataFineSottoscrizione));
                    cmd.Parameters.Add(new SqlParameter("@DataFineQuota", utente.DataFineQuota));
                    cmd.ExecuteNonQuery();

                    return true;
                }
                //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                return false;
            }
        }

        public static bool CheckUsername(string username)
        {
            
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_chk_Username", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Username", username));
                    cmd.Parameters.Add("@retValue", System.Data.SqlDbType.BigInt).Direction =
                                    ParameterDirection.ReturnValue;
                    cmd.ExecuteNonQuery();
                    if (Convert.ToInt64(cmd.Parameters["@retValue"].Value) > 0) return true;

                    return false;
                }
                //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                return false;
            }
        }

        public static string GetValue(long id, string table, string descrCol = "Descrizione")
        {
            
            //SqlDataReader rdr = null;
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    var cmd = new SqlCommand("proc_sel_Value", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@id", id));
                    cmd.Parameters.Add(new SqlParameter("@Table", table));
                    cmd.Parameters.Add(new SqlParameter("@DescrCol", descrCol));
                    SqlDataReader sdr = cmd.ExecuteReader();
                    if (sdr.Read())
                    {
                        return Convert.ToString(sdr[descrCol]);
                    }
                    return null;
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<FatturaModel> getFattureDaIdSdi(long identificativoSdI)
        {
            
            List<FatturaModel> fatture = new List<FatturaModel>();
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();

                    SqlCommand cmd = new SqlCommand("proc_sel_FattureAttiveIdSDI", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSdI", identificativoSdI));
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        FatturaModel fattura = new FatturaModel();
                        if (sdr["id"] != DBNull.Value) fattura.Id = (long)sdr["id"];
                        if (sdr["idUser"] != DBNull.Value) fattura.IdUser = (long)sdr["idUser"];
                        if (sdr["NomeFile"] != DBNull.Value) fattura.NomeFile = (string)sdr["NomeFile"];
                        if (sdr["ProgressivoInvio"] != DBNull.Value) fattura.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
                        if (sdr["Denominazione"] != DBNull.Value) fattura.Denominazione = (string)sdr["Denominazione"];
                        if (sdr["IdFiscaleIVA"] != DBNull.Value) fattura.IdFiscaleIva = (string)sdr["IdFiscaleIVA"];
                        if (sdr["FormatoTrasmissione"] != DBNull.Value) fattura.FormatoTrasmissione = (string)sdr["FormatoTrasmissione"];
                        if (sdr["TipoDocumento"] != DBNull.Value) fattura.TipoDocumento = (string)sdr["TipoDocumento"];
                        if (sdr["NumeroFattura"] != DBNull.Value) fattura.NumeroFattura = (string)sdr["NumeroFattura"];
                        if (sdr["Data"] != DBNull.Value) fattura.Data = (DateTime)sdr["Data"];
                        if (sdr["ImportoTotaleDocumento"] != DBNull.Value) fattura.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
                        if (sdr["NomeFileMetadati"] != DBNull.Value) fattura.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
                        if (sdr["DataPagamento"] != DBNull.Value) fattura.DataPagamento = (DateTime)sdr["DataPagamento"];
                        if (sdr["idStato"] != DBNull.Value) fattura.IdStato = (short)sdr["idStato"];
                        if (sdr["IdentificativoSdI"] != DBNull.Value) fattura.IdentificativoSDI = (long)sdr["IdentificativoSdI"];
                        if (sdr["DataCreate"] != DBNull.Value) fattura.DataCreate = (DateTime)sdr["DataCreate"];
                        if (sdr["IpCreate"] != DBNull.Value) fattura.IpCreate = (string)sdr["IpCreate"];
                        if (sdr["NoteInterne"] != DBNull.Value) fattura.NoteInterne = (string)sdr["NoteInterne"];
                        //if (sdr["StatoMittBreve"] != DBNull.Value) fattura.StatoMittBreve = (string)sdr["StatoMittBreve"];
                        //if (sdr["StatoMittLungo"] != DBNull.Value) fattura.StatoMittLungo = (string)sdr["StatoMittLungo"];
                        if (sdr["idLotto"] != DBNull.Value) fattura.idLotto = (long)sdr["idLotto"];

                        fatture.Add(fattura);
                    }
                    return fatture;
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception)
            {
                return fatture;
            }
        }

        public static List<FatturaModel> getFatturePassiveDaIdSdi(long identificativoSdI)
        {
            
            List<FatturaModel> fatture = new List<FatturaModel>();
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();

                    SqlCommand cmd = new SqlCommand("proc_sel_FatturePassiveIdSDI", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSdI", identificativoSdI));
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        FatturaModel fattura = new FatturaModel();
                        if (sdr["id"] != DBNull.Value) fattura.Id = (long)sdr["id"];
                        if (sdr["idUser"] != DBNull.Value) fattura.IdUser = (long)sdr["idUser"];
                        if (sdr["NomeFile"] != DBNull.Value) fattura.NomeFile = (string)sdr["NomeFile"];
                        if (sdr["ProgressivoInvio"] != DBNull.Value) fattura.ProgressivoInvio = (string)sdr["ProgressivoInvio"];
                        if (sdr["Denominazione"] != DBNull.Value) fattura.Denominazione = (string)sdr["Denominazione"];
                        if (sdr["IdFiscaleIVA"] != DBNull.Value) fattura.IdFiscaleIva = (string)sdr["IdFiscaleIVA"];
                        if (sdr["FormatoTrasmissione"] != DBNull.Value) fattura.FormatoTrasmissione = (string)sdr["FormatoTrasmissione"];
                        if (sdr["TipoDocumento"] != DBNull.Value) fattura.TipoDocumento = (string)sdr["TipoDocumento"];
                        if (sdr["NumeroFattura"] != DBNull.Value) fattura.NumeroFattura = (string)sdr["NumeroFattura"];
                        if (sdr["Data"] != DBNull.Value) fattura.Data = (DateTime)sdr["Data"];
                        if (sdr["ImportoTotaleDocumento"] != DBNull.Value) fattura.ImportoTotaleDocumento = (decimal)sdr["ImportoTotaleDocumento"];
                        if (sdr["NomeFileMetadati"] != DBNull.Value) fattura.NomeFileMetadati = (string)sdr["NomeFileMetadati"];
                        if (sdr["DataPagamento"] != DBNull.Value) fattura.DataPagamento = (DateTime)sdr["DataPagamento"];
                        if (sdr["idStato"] != DBNull.Value) fattura.IdStato = (short)sdr["idStato"];
                        if (sdr["IdentificativoSdI"] != DBNull.Value) fattura.IdentificativoSDI = (long)sdr["IdentificativoSdI"];
                        if (sdr["DataCreate"] != DBNull.Value) fattura.DataCreate = (DateTime)sdr["DataCreate"];
                        if (sdr["IpCreate"] != DBNull.Value) fattura.IpCreate = (string)sdr["IpCreate"];
                        if (sdr["NoteInterne"] != DBNull.Value) fattura.NoteInterne = (string)sdr["NoteInterne"];
                        //if (sdr["StatoMittBreve"] != DBNull.Value) fattura.StatoMittBreve = (string)sdr["StatoMittBreve"];
                        //if (sdr["StatoMittLungo"] != DBNull.Value) fattura.StatoMittLungo = (string)sdr["StatoMittLungo"];
                        if (sdr["idLotto"] != DBNull.Value) fattura.idLotto = (long)sdr["idLotto"];

                        fatture.Add(fattura);
                    }
                    return fatture;
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception)
            {
                return fatture;
            }
        }

        public static void CambiaStatoFatture(List<FatturaModel> fatture, short stato)
        {
            
            SqlTransaction myTran = null;
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    myTran = con.BeginTransaction();
                    //SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDI", con);
                    //cmd.Transaction = myTran;
                    //cmd.CommandType = CommandType.StoredProcedure;

                    //cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    ////cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    //cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.NOTIFICA_RICEVUTA_CONSEGNA_TYPE));
                    //cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    //cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    //cmd.Parameters.Add(new SqlParameter("@Titolo", Const.RICEVUTA_CONSEGNA_TITLE));
                    //cmd.Parameters.Add(new SqlParameter("@Descrizione", Const.RICEVUTA_CONSEGNA_DESCRIPTION));
                    //cmd.Parameters.Add(new SqlParameter("@Note", note));
                    //cmd.ExecuteNonQuery();

                    foreach (FatturaModel fattura in fatture)
                    {
                        SqlCommand cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", stato));
                        cmd.ExecuteNonQuery();
                    }


                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
        }

        public static void NotificaMancataConsegna(long identificativoSdI, List<FatturaModel> fatture, string nomeFile, string descrizione = null, string note = null)
        {
            
            SqlTransaction myTran = null;
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    myTran = con.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDI", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.NOTIFICA_MANCATA_CONSEGNA_TYPE));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    //cmd.Parameters.Add(new SqlParameter("@Descrizione", fattura.Descrizione));
                    cmd.Parameters.Add(new SqlParameter("@Note", note));
                    cmd.ExecuteNonQuery();

                    foreach (FatturaModel fattura in fatture)
                    {
                        cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", Const.NON_CONSEGNATA_STATE));
                        cmd.ExecuteNonQuery();
                    }


                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
        }

        public static void inserisciNotifica(List<FatturaModel> fatture, MessaggioSDI messaggio)
        {
            
            SqlTransaction myTran = null;
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    myTran = con.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDI", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", messaggio.IdentificativoSDI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", messaggio.idTipoMessaggio));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", messaggio.NomeFileNotifica));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", messaggio.DataRicezione));
                    cmd.Parameters.Add(new SqlParameter("@Titolo", messaggio.Titolo));
                    cmd.Parameters.Add(new SqlParameter("@Descrizione", messaggio.Descrizione));
                    cmd.Parameters.Add(new SqlParameter("@Note", messaggio.Note));
                    cmd.Parameters.Add("@retValue", System.Data.SqlDbType.BigInt).Direction =
                                    ParameterDirection.ReturnValue;


                    cmd.ExecuteNonQuery();

                    long idMessaggio = Convert.ToInt64(cmd.Parameters["@retValue"].Value);

                    foreach (ErroriSDI errore in messaggio.Errori)
                    {
                        cmd = new SqlCommand("proc_ins_MessaggiSDI_ErroriSDI", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@idMessaggioSDI", idMessaggio));
                        cmd.Parameters.Add(new SqlParameter("@Codice", errore.Codice));
                        cmd.Parameters.Add(new SqlParameter("@Descrizione", errore.Descrizione));
                        cmd.Parameters.Add(new SqlParameter("@Suggerimento", errore.Suggerimento));
                        //cmd.Parameters.Add(new SqlParameter("@Ordine", Const.NOTIFICA_SCARTO_STATE));
                        cmd.ExecuteNonQuery();
                    }

                    foreach (FatturaModel fattura in fatture)
                    {
                        cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", Const.SCARTATA_STATE));
                        cmd.ExecuteNonQuery();
                    }




                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
        }

        public static void NotificaEsito(long identificativoSdI, string esito, List<FatturaModel> fatture, string nomeFile, string descrizione = null, string note = null)
        {
            
            SqlTransaction myTran = null;
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDI", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.NOTIFICA_ESITO_TYPE));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    cmd.Parameters.Add(new SqlParameter("@Descrizione", descrizione));
                    //cmd.Parameters.Add(new SqlParameter("@Note", note));
                    cmd.ExecuteNonQuery();

                    foreach (FatturaModel fattura in fatture)
                    {
                        cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", Const.NON_CONSEGNATA_STATE));
                        cmd.ExecuteNonQuery();
                    }


                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
        }

        public static void NotificaDecorrenzaTermini(long identificativoSdI, List<FatturaModel> fatture, List<ErroriSDI> errori, string nomeFile, string descrizione = null, string note = null)
        {
            
            SqlTransaction myTran = null;
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDI", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter retValParam = new SqlParameter("@RETURN_VALUE", SqlDbType.BigInt);
                    retValParam.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(retValParam);
                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.NOTIFICA_DECORRENZA_TERMINI_TYPE));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    //cmd.Parameters.Add(new SqlParameter("@Descrizione", fattura.Descrizione));
                    cmd.Parameters.Add(new SqlParameter("@Note", note));
                    SqlDataReader reader = cmd.ExecuteReader();

                    long idMessaggio = Convert.ToInt32(retValParam.Value);
                    reader.Close();
                    cmd.Parameters.Clear();

                    if (errori != null)
                        foreach (ErroriSDI errore in errori)
                        {
                            cmd = new SqlCommand("proc_ins_MessaggiSDI_ErroriSDI", con);
                            cmd.Transaction = myTran;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@idMessaggioSDI", idMessaggio));
                            cmd.Parameters.Add(new SqlParameter("@Codice", errore.Codice));
                            cmd.Parameters.Add(new SqlParameter("@Descrizione", errore.Descrizione));
                            cmd.Parameters.Add(new SqlParameter("@Suggerimento", errore.Suggerimento));
                            //cmd.Parameters.Add(new SqlParameter("@Ordine", Const.NOTIFICA_SCARTO_STATE));
                            cmd.ExecuteNonQuery();
                        }

                    if (fatture != null)
                        foreach (FatturaModel fattura in fatture)
                        {
                            cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                            cmd.Transaction = myTran;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                            cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                            cmd.Parameters.Add(new SqlParameter("@IdStato", Const.DECORRENZA_TERMINI_STATE));
                            cmd.ExecuteNonQuery();
                        }




                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
        }

        public static void AttestazioneTrasmissioneFattura(long identificativoSdI, List<FatturaModel> fatture, List<ErroriSDI> errori, string nomeFile, string descrizione = null, string note = null)
        {
            
            SqlTransaction myTran = null;
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("proc_ins_MessaggiSDI", con);
                    cmd.Transaction = myTran;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter retValParam = new SqlParameter("@RETURN_VALUE", SqlDbType.BigInt);
                    retValParam.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(retValParam);
                    cmd.Parameters.Add(new SqlParameter("@IdentificativoSDI", identificativoSdI));
                    //cmd.Parameters.Add(new SqlParameter("@idFattAtt", fattura.idFattAtt));
                    cmd.Parameters.Add(new SqlParameter("@idTipoMessaggio", Const.NOTIFICA_ATTESTAZIONE_TRASMISSIONE_FATTURA_TYPE));
                    cmd.Parameters.Add(new SqlParameter("@NomeFile", nomeFile));
                    cmd.Parameters.Add(new SqlParameter("@DataRicezione", DateTime.Now));
                    //cmd.Parameters.Add(new SqlParameter("@Descrizione", fattura.Descrizione));
                    cmd.Parameters.Add(new SqlParameter("@Note", note));
                    SqlDataReader reader = cmd.ExecuteReader();

                    long idMessaggio = Convert.ToInt32(retValParam.Value);
                    reader.Close();
                    cmd.Parameters.Clear();

                    foreach (ErroriSDI errore in errori)
                    {
                        cmd = new SqlCommand("proc_ins_MessaggiSDI_ErroriSDI", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@idMessaggioSDI", idMessaggio));
                        cmd.Parameters.Add(new SqlParameter("@Codice", errore.Codice));
                        cmd.Parameters.Add(new SqlParameter("@Descrizione", errore.Descrizione));
                        cmd.Parameters.Add(new SqlParameter("@Suggerimento", errore.Suggerimento));
                        //cmd.Parameters.Add(new SqlParameter("@Ordine", Const.NOTIFICA_SCARTO_STATE));
                        cmd.ExecuteNonQuery();
                    }

                    foreach (FatturaModel fattura in fatture)
                    {
                        cmd = new SqlCommand("proc_upd_StatoFatturaAttiva", con);
                        cmd.Transaction = myTran;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@id", fattura.Id));
                        cmd.Parameters.Add(new SqlParameter("@idUser", fattura.IdUser));
                        cmd.Parameters.Add(new SqlParameter("@IdStato", Const.ATTESA_ACCETTAZIONE_STATE));
                        cmd.ExecuteNonQuery();
                    }

                    myTran.Commit();
                    //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
                }
            }
            catch (Exception)
            {
                //GlobalUtils.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception)
                    {
                        //GlobalUtils.WriteException(ex2);
                    }
            }
        }


        public static User checkCustomer(CessionarioCommittente cessionarioCommittente)
        {
            
            try
            {
                //controllo il formato del cessionario committente
                if (cessionarioCommittente.DatiAnagrafici.IdFiscaleIVA == null &&
                    cessionarioCommittente.DatiAnagrafici.CodiceFiscale == null)
                    return null;
                string idFiscale = null;
                if (cessionarioCommittente.DatiAnagrafici.IdFiscaleIVA != null)
                {
                    idFiscale = cessionarioCommittente.DatiAnagrafici.IdFiscaleIVA.IdPaese +
                                cessionarioCommittente.DatiAnagrafici.IdFiscaleIVA.IdCodice;
                }
                else
                {
                    idFiscale = cessionarioCommittente.DatiAnagrafici.CodiceFiscale;
                }

                User user = new User(idFiscale);
                return (user.id > 0 ? user : null);

                //if (DebugMode > 5) GlobalUtils.WriteLog("scrivi messaggio qui");
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                return null;
            }
        }

        public static long InserisciFatturaPassiva(Fattura fattura, FatturaModel model)
        {
            
            SqlTransaction myTran = null;
            try
            {
                using (SqlConnection con = ConfManager.GetDbConnection())
                {
                    long idFat = -1;
                    var numeroFat = fattura.FatturaElettronicaBody[0].DatiGenerali.DatiGeneraliDocumento.Numero;
                    var dataFat = fattura.FatturaElettronicaBody[0].DatiGenerali.DatiGeneraliDocumento.Data;

                SqlCommand command = new SqlCommand("proc_sel_FatturePassive", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = myTran;
                command.Parameters.AddWithValue("NumeroFattura", numeroFat);
                command.Parameters.AddWithValue("DataFattura", dataFat);
                command.Parameters.AddWithValue("idUser", model.IdUser);
                    using (SqlDataReader rdr = command.ExecuteReader())
                    {
                        if (rdr != null && rdr.Read())
                        {
                            //la fattura esiste già
                            return -1;
                        }
                        else
                        {
                            command = new SqlCommand("proc_ins_FatturaPassiva", con);
                            command.CommandType = CommandType.StoredProcedure;
                            command.Transaction = myTran;
                            command.Parameters.AddWithValue("idUser", model.IdUser);
                            command.Parameters.AddWithValue("NomeFile", model.NomeFile);
                            command.Parameters.AddWithValue("NomeFileMetadati", model.NomeFileMetadati);
                            command.Parameters.AddWithValue("ProgressivoInvio",
                                                            fattura.FatturaElettronicaHeader.DatiTrasmissione.ProgressivoInvio);
                            if (fattura.FatturaElettronicaHeader.CessionarioCommittente.DatiAnagrafici.Anagrafica.Denominazione == null)
                                command.Parameters.AddWithValue("Denominazione",
                                                                fattura.FatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici
                                                                       .Anagrafica.CognomeNome);
                            else command.Parameters.AddWithValue("Denominazione",
                                                            fattura.FatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici
                                                                   .Anagrafica.Denominazione);
                            command.Parameters.AddWithValue("IdFiscaleIVA",
                                                            fattura.FatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici
                                                                   .IdFiscaleIVA.IdCodice +
                                                            fattura.FatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici
                                                                   .IdFiscaleIVA.IdPaese);
                            //command.Parameters.AddWithValue("FormatoTrasmissione", FormatoTrasmissione);
                            command.Parameters.AddWithValue("TipoDocumento",
                                                            fattura.FatturaElettronicaBody[0].DatiGenerali.DatiGeneraliDocumento
                                                                .TipoDocumento);
                            command.Parameters.AddWithValue("NumeroFattura",
                                                            fattura.FatturaElettronicaBody[0].DatiGenerali.DatiGeneraliDocumento.Numero);
                            command.Parameters.AddWithValue("Data", fattura.FatturaElettronicaBody[0].DatiGenerali.DatiGeneraliDocumento.Data);
                            //if (fattura.FatturaElettronicaBody[0].DatiPagamento.Count > 0 &&
                            //    fattura.FatturaElettronicaBody[0].DatiPagamento[0].DettaglioPagamento.Count > 0)
                            //    command.Parameters.AddWithValue("ImportoTotaleDocumento",
                            //                                    fattura.FatturaElettronicaBody[0].DatiPagamento[0].DettaglioPagamento[0]
                            //                                        .ImportoPagamento);
                            //else
                            decimal importo = 0;
                            foreach (var dettaglio in fattura.FatturaElettronicaBody[0].DatiBeniServizi.DatiRiepilogo)
                            {
                                importo += (dettaglio.ImponibileImporto + dettaglio.Imposta);
                            }
                            command.Parameters.AddWithValue("ImportoTotaleDocumento", importo);

                            command.Parameters.AddWithValue("idStato", 1);
                            command.Parameters.AddWithValue("DataCreate", DateTime.Now);
                            command.Parameters.AddWithValue("IpCreate", MyUtils.GetIPv4Address());
                            command.Parameters.AddWithValue("idLotto", 0);
                            command.Parameters.Add("@retValue", System.Data.SqlDbType.BigInt).Direction =
                                ParameterDirection.ReturnValue;
                            command.ExecuteNonQuery();
                            idFat = Convert.ToInt64(command.Parameters["@retValue"].Value);

                            foreach (var riga in fattura.FatturaElettronicaBody[0].DatiBeniServizi.DettaglioLinee)
                            {
                                command = new SqlCommand("proc_ins_FatturaRigPassiva", con);
                                command.CommandType = CommandType.StoredProcedure;
                                command.Transaction = myTran;
                                command.Parameters.AddWithValue("idFatt", idFat);
                                command.Parameters.AddWithValue("Descrizione", riga.Descrizione);
                                command.Parameters.AddWithValue("Quantita",
                                                                riga.Quantita);
                                command.Parameters.AddWithValue("Importo", riga.PrezzoTotale);
                                command.ExecuteNonQuery();
                            }
                        }
                        myTran.Commit();
                        return idFat;
                    }
                }
            }
            catch (Exception ex)
            {
                LogWriter.WriteException(ex);
                if (myTran != null && myTran.Connection != null)
                    try
                    {
                        myTran.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        LogWriter.WriteException(ex2);
                    }
                return -1;
            }
        }
    }
}