﻿using System;
using System.Collections.Generic;

namespace RestWS.Models
{
    public class Lotto
    {
        public long Id { get; set; }

        public long IdUser { get; set; }

        public string NomeFile { get; set; }

        public string ProgressivoInvio { get; set; }

        public string FormatoTrasmissione { get; set; }

        public string Denominazione { get; set; }

        public string IdFiscaleIva { get; set; }

        public Decimal ImportoTotaleDocumento { get; set; }

        public DateTime DataCreate { get; set; }

        public string NomeFileMetadati { get; set; }

        public short IdStato { get; set; }

        public string StatoMittBreve { get; set; }

        public string StatoMittLungo { get; set; }

        public string StatoDestBreve { get; set; }

        public string StatoDestLungo { get; set; }

        public string NoteInterne { get; set; }

        public List<FatturaModel> Fatture { get; set; }
    }
}