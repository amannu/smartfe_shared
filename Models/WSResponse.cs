﻿namespace RestWS.Models
{
    public class WSResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }
        public object Data { get; set; }

        public WSResponse() { }

        public WSResponse(bool success, string message, int code, object data)
        {
            Success = success;
            Message = message;
            Code = code;
            Data = data;
        }
    }
}